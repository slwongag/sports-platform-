export class AlgoService {

    public eloAlgo(myScore: number, competitorScore: number, mySetNum: number, competitorSetNum: number, formOfMatch: string, firstTen: number) {
        let result: number = 0;
        let bonus: number = 0;
        let weightConstant: number = 0;
        let resultWeight: number = 0;
        let myExpectation: number = 1 / (1 + 10 ** ((competitorScore - myScore) / 400));
        if (mySetNum - competitorSetNum > 0) {
            result = 1;
        }
        else if (mySetNum - competitorSetNum < 0) {
            result = -1;
        }
        if (formOfMatch === "Second" || formOfMatch === "Third") {
            if (mySetNum - competitorSetNum == 2 || mySetNum - competitorSetNum == -2) {
                bonus = 0.5
            }
        }
        else if (formOfMatch === "First") {
            if (mySetNum - competitorSetNum == 6 || mySetNum - competitorSetNum == -6) {
                bonus = 5 / 6;
            }
            else if (mySetNum - competitorSetNum == 5 || mySetNum - competitorSetNum == -5) {
                bonus = 4 / 6;
            }
            else if (mySetNum - competitorSetNum == 4 || mySetNum - competitorSetNum == -4) {
                bonus = 3 / 6;
            }
            else if (mySetNum - competitorSetNum == 3 || mySetNum - competitorSetNum == -3) {
                bonus = 2 / 6;
            }
            else if (mySetNum - competitorSetNum == 2 || mySetNum - competitorSetNum == -2) {
                bonus = 1 / 6;
            }
        }

        if (formOfMatch === "First") {
            weightConstant = 10 * (1 + bonus);
        }
        else if (formOfMatch === "Second") {
            weightConstant = 15 * (1 + bonus);
        }
        else if (formOfMatch === "Third") {
            weightConstant = 20 * (1 + bonus);
        }
        if (result === 1) {
            resultWeight = 1;
        }
        else if (result === -1) {
            resultWeight = 0;
        }
        if (firstTen <= 10) {
            if (result == -1 && myScore > competitorScore) {
                let ans: number = myScore + 10 * weightConstant * (resultWeight - myExpectation);
                console.log("hi1")
                return Math.round(ans);
            }
            else if (result == 1 && myScore < competitorScore) {
                let ans: number = myScore + 10 * weightConstant * (resultWeight - myExpectation);
                console.log("hi2")
                return Math.round(ans);
            }
            else {
                console.log("hi3")
                let ans: number = myScore + weightConstant * (resultWeight - myExpectation);
                console.log(ans);
                return Math.round(ans);
            }
        }
        else {
            console.log("hi4")
            console.log(weightConstant)
            let ans: number = myScore + weightConstant * (resultWeight - myExpectation);
            console.log(ans);
            Math.round(ans)
            return Math.round(ans);
        }
    }
} 