import { playerService } from "../app";
import { PlayerService, Player } from "./PlayerService";



export class MatchService {
    private players: PlayerService 

    constructor(){
        this.players = playerService
    }

    // [CODE REVIEW] Example of filtering
    protected _filterSport(chosenSport: string) {
        return (player: Player) => {
            for (let sportEvery of player.sports) {
                if (chosenSport == sportEvery.sport) {
                    return true;
                }
            }
            return false;
        };
    }
    protected _filterGender(chosenGender: string) {
        return (player: Player) => {
            if (chosenGender == player.gender) {
                return true;
            }
            return false;
        };
    }
    // end

    public filterSport(chosenSport: string,players: Player[]) {
        let filteredPlayers: Player[] = [];
        for (let player of players) {
            for (let sportEvery of player.sports) {
                if (chosenSport == sportEvery.sport) {
                    filteredPlayers.push(player)
                }
            }
        }
        return filteredPlayers;
    }

    public filterGender(chosenGender: string, players: Player[]) {
        let filteredPlayers: Player[] = [];
        for (let player of players) {
            if (chosenGender == player.gender) {
                filteredPlayers.push(player)
            }
        }
        return filteredPlayers;
    }

    public filterAgeRange(minimum: number, maximum: number, players: Player[]) {
        let filteredPlayers: Player[] = [];
        for (let player of players) {
            if (player.age >= minimum && player.age <= maximum) {
                filteredPlayers.push(player)
            }
        }
        return filteredPlayers;
    }

    public filterAvailability(availability:number[][],players:Player[]){
        let filteredPlayers: Player[] = [];
        for (let i=0;i<7;i++){
            for (let j=0;j<2;j++){
                if (availability[i][j]==1){
                    for (let player of players) {
                        if (player.availability[i][j]==1 && this.checkRepeated(filteredPlayers,player)==false){
                            filteredPlayers.push(player)
                        }
                    }
                }
            }
        }
        return filteredPlayers;
    }
    public checkRepeated(players:Player[],pickPlayer:Player){
        for (let player of players){
            if (pickPlayer.playerId ==player.playerId){
                return true;
            }
        }
        return false;
    }
    public getProficiency(chosenSport: string, player: Player) {
        for (let sport of player.sports) {
            if (sport.sport == chosenSport) {
                return sport.proficiency;
            }
        }
        return 0;
    }
    public getMatchNo(player: Player, chosenSport: string) {
        for (let sport of player.sports) {
            if (sport.sport == chosenSport) {
                return sport.record.match;
            }
        }
        return 0;
    }

    public filterProficiency(chosenSport: string, myPlayerId: string, players: Player[]) {
        let filteredPlayers: Player[] = [];
        let myMatchNumber: number
        let myPlayer: Player | undefined
        myPlayer = this.players.getPlayer(myPlayerId);
        if (myPlayer !== undefined) {
            if (this.getMatchNo(myPlayer, chosenSport) !== undefined) {
                myMatchNumber = this.getMatchNo(myPlayer, chosenSport)
            }
            else (
                myMatchNumber = 0
            )
            for (let player of players) {
                if (player.playerId != myPlayerId) {
                    for (let sport of player.sports) {
                        if (sport.sport === chosenSport) {
                            if (myMatchNumber <= 10) {
                                if ((this.getProficiency(chosenSport, myPlayer) - 2 *100) <= sport.proficiency || (sport.proficiency + 2 * 100) >= this.getProficiency(chosenSport, myPlayer)) {
                                    filteredPlayers.push(player)
                                }
                            }
                            else {
                                // console.log('hi')
                                if ((this.getProficiency(chosenSport, myPlayer) - 100) <= sport.proficiency || (sport.proficiency + 100) >= this.getProficiency(chosenSport, myPlayer)) {
                                    filteredPlayers.push(player)
                                }
                            }
                            // [CODE REVIEW] break;
                        }
                    }
                }
            }
        }
        return filteredPlayers;
    }

    public getLatitude(player: Player) {
        return player.location[0];
    }

    public getLongitude(player: Player) {
        return player.location[1];
    }
    private deg2rad(deg: number) {
        return deg * (Math.PI / 180)
    }

    public calculateDistance(myLatitude: number, myLongitude: number, latitude: number, longitude: number) {
        var R: number = 6371; // Radius of the earth in km
        var dLat: number = this.deg2rad(myLatitude - latitude);  // deg2rad below
        var dLon: number = this.deg2rad(myLongitude - longitude);
        var a: number =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(myLatitude)) * Math.cos(this.deg2rad(latitude)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
            ;
        var c: number = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d: number = R * c; // Distance in km
        return d;
    }

    public filterByDistance(myPlayerId: string, players: Player[], chosenDistance:number) {
        let filteredPlayers: Player[] = [];
        let myPlayer: Player | undefined
        myPlayer = this.players.getPlayer(myPlayerId);
        if (myPlayer !== undefined) {
            let myLatitude: number = this.getLatitude(myPlayer);
            let myLongitude: number = this.getLongitude(myPlayer)
            for (let player of players) {
                if (player.playerId != myPlayerId) {
                    let compareLatitude: number = this.getLatitude(player);
                    let compareLongitude: number = this.getLongitude(player);
                    let distance = this.calculateDistance(myLatitude, myLongitude, compareLatitude, compareLongitude)
                    if (distance <= chosenDistance) {
                        filteredPlayers.push(player)
                    }
                }
            }
        }
        return filteredPlayers;
    }

    public filter(chosenSport: string, chosenGender: string, minimum: number, maximum: number, myPlayerId: string,chosenDistance: number, availability:number[][]) {
        // [CODE REVIEW] example
        // let _filteredResult = this.players.getPlayers()
        //         .filter(this._filterSport(chosenSport))
        //         .filter(this._filterGender(chosenGender))
        // end

        // [CODE REVIEW] another example
        // const query:PlayerQuery = this.getPlayerQuery();
        // query.filterGender(gender);
        // query.filterSport(sport)
        // const players = query.toList();
        //
        let filteredResult: Player[] = this.filterByDistance(myPlayerId, this.filterProficiency(chosenSport, myPlayerId, this.filterAgeRange(minimum, maximum, this.filterGender(chosenGender, this.filterAvailability(availability,this.filterSport(chosenSport,this.players.getPlayers())) ))), chosenDistance)
        // console.log(filteredResult.length)
        return filteredResult;
    }

    public setSortNumber (me:Player,chosenSport: string,players:Player[],weightingOfProficiency:number,weightingOfDistance:number){
        for (let player of players){
            player.sortNumber=weightingOfProficiency*Math.abs(this.getProficiency(chosenSport,player)-this.getProficiency(chosenSport,me))/100+weightingOfDistance*this.calculateDistance(this.getLatitude(me),this.getLongitude(me),this.getLatitude(player),this.getLongitude(player)/200)
        }
        return players
    }

    // public compare( me:Player, others:Player ) {
    //     if ( me.sortNumber < others.sortNumber ){
    //       return -1;
    //     }
    //     if ( me.sortNumber > others.sortNumber  ){
    //       return 1;
    //     }
    //     return 0;
    //   }


    public filterThenSort(chosenSport: string, chosenGender: string, minimum: number, maximum: number, myPlayerId: string,chosenDistance: number,availability:number[][],weightingOfProficiency:number,weightingOfDistance:number) {
        let myPlayer: Player | undefined
        myPlayer = this.players.getPlayer(myPlayerId);
        if (myPlayer!=undefined){
            return this.setSortNumber(myPlayer,chosenSport,this.filter(chosenSport, chosenGender, minimum, maximum, myPlayerId,chosenDistance,availability),weightingOfProficiency,weightingOfDistance).sort((a:Player,b:Player)=>{if ( a.sortNumber < b.sortNumber ){
                return -1;
              }
              if ( a.sortNumber > b.sortNumber  ){
                return 1;
              }
              return 0;})
        }
        else {
            return [];
        }
    }


}






