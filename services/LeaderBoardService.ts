import { playerService } from "../app";
import { PlayerService, Player } from "./PlayerService";

export class LeaderBoardService {
    public AllPlayers: PlayerService = playerService
    protected Rank: Player[]


    // public RankPlayersBySport(sport:string){
    //     const player = this.AllPlayers.getPlayers();
    //     const playerBySport = player.filter(playerBySport =>playerBySport.sports[0].sport === sport)
    //     console.log(playerBySport);
    //     const rank = playerBySport.sort(function(a:Player,b:Player){
    //         return b.sports[0].proficiency > a.sports[0].proficiency ? 1 : -1;
    //     })
    //     return rank;
    // }

    public ListAllPlayersBySports(sport: string) {
        const players = this.AllPlayers.getPlayers();
        const playersBySport: Player[] = [];
        for (let player of players) {
            for (let chosenSport of player.sports) {
                if (chosenSport.sport === sport) {
                    playersBySport.push(player)
                }
            }
        }
        return playersBySport;
    }

    public RankAllPlayerByProficiency(sport: string, players: Player[]) {
        players.sort(function (a: Player, b: Player) {
            for (let i = 0; i < a.sports.length; i++) {
                if (sport === a.sports[i].sport) {
                    let aIndex: number = i
                    for (let j = 0; j < b.sports.length; j++) {
                        if (sport === a.sports[j].sport) { // [CODE REVIEW] should be b.sports[j].sport ?
                            let bIndex: number = j
                            //     b.sports[bIndex].proficiency > a.sports[aIndex].proficiency ? 1 : -1;
                            // [CORE REVIEW] Have to return "0" on equal case
                            return b.sports[aIndex].proficiency > a.sports[bIndex].proficiency ? 1 : -1;
                        }
                    }
                }
            }
            // [CORE REVIEW] Should have return "0" 
            return 1;

        })
        return players;
    }

    public RankAllPlayerBySportAndProficiency(sport: string) {
        const rank = this.RankAllPlayerByProficiency(sport, this.ListAllPlayersBySports(sport))
        return rank;
    }
}
// Another way
//     public RankAllPlayersBySports(sport:string){
//         const players = this.AllPlayers.getPlayers();
//         const playersBySport:Player[] =[];
//         for (let player of players){
//             for (let chosenSport of player.sports){
//                 if (chosenSport.sport === sport){
//                     playersBySport.push(player)
//                 }
//             }
//         }  

//         const rank = playersBySport.sort(function(a:Player,b:Player){  
//             for (let i = 0; i < a.sports.length; i++){
//                 if (sport === a.sports[i].sport){
//                     let aIndex:number = i
//                     for (let j = 0; j < b.sports.length; j++){
//                         if (sport === a.sports[j].sport){
//                             let bIndex:number = j
//                             return b.sports[aIndex].proficiency > a.sports[bIndex].proficiency ? 1 : -1;
//                         }
//                     }
//                 }
//             }
//             return 1;

//                 })
//                 return rank;
//     }

//     public RankAllPlayerBySports(sport:string){


//     }

// }

// // Test code
// const newRank = new LeaderBoardService;
// // newRank.RankAllPlayersBySports('tennis')
// // console.log(newRank.RankAllPlayersBySports('tennis'))

// console.log(newRank.RankAllPlayerBySportAndProficiency('tennis'))

