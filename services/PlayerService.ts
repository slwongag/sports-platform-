import * as jsonfile from "jsonfile";

export type Availability = [
    [number, number], 
    [number, number], 
    [number, number], 
    [number, number], 
    [number, number], 
    [number, number], 
    [number, number]
]

export type Location = [number, number]

export interface Contacts {
    phone: number, 
    email: string
}

export interface Record {
    match: number, 
    win: number, 
    lose: number
}

export interface Sport {
    sport: string, 
    proficiency: number, 
    record: Record
}

export interface Social {
    playerId: string, 
    playerName: string, 
    imgSrc: string
}

export interface PublicProfile {
    playerId: string, 
    playerName: string, 
    age: number, 
    gender: string, 
    contacts: Contacts, 
    sports: Sport[], 
    imgSrc: string
}

export interface Player {
    playerId: string, 
    playerName: string, 
    age: number, 
    gender: string, 
    contacts: Contacts, 
    availability: Availability, 
    location: Location, 
    sports: Sport[], 
    imgSrc: string, 
    sortNumber: number
}

export class PlayerService {

    protected players: Player[];
    // public calRank:AlgoService = new AlgoService();

    constructor() {
        this.loadPlayers();
    }

    public loadPlayers() {
        this.players = jsonfile.readFileSync(__dirname + "/players.json");
        return this.players;
    }

    public loadPlayersSocial() {
        let playersSocial: Social[] = [];
        this.players = jsonfile.readFileSync(__dirname + "/players.json");
        for (let player of this.players) {
            playersSocial.push({
                playerId: player.playerId, 
                playerName: player.playerName, 
                imgSrc: player.imgSrc
            })
        }
        return playersSocial;
    }

    private updatePlayers() {
        jsonfile.writeFileSync(__dirname + "/players.json", this.players);
        // console.log("this.players = ", this.players);
        return this.players;
    }

    public getPlayers() {
        return this.players;
    }

    public getPlayer(playerId: string) {
        const player = this.players.find(player => player.playerId === playerId);
        // console.log("player = ", player);
        return player;
    }

    public getPublicProfile(playerId: string) {
        const player = this.players.find(player => player.playerId === playerId);
        if (player) {
            const publicProfile: PublicProfile = {
                playerId: player.playerId, 
                playerName: player.playerName, 
                age: player.age, 
                gender: player.gender, 
                contacts: player.contacts, 
                sports: player.sports, 
                imgSrc: player.imgSrc
            }
            return publicProfile;
        } else {
            console.log("Invalid playerId");
            throw new Error("Invalid playerId");
        }
    }

    public addPlayer(newPlayer: Player) {
        const player = this.players.find(player => player.playerId === newPlayer.playerId);
        if (player) {
            console.log(newPlayer.playerId);
            console.log("this.players = ", this.players);
            throw new Error("playerId already exists");
        } else {
            console.log("newPlayer ", newPlayer);
            this.players.push(newPlayer);
            this.updatePlayers();
        }
    }

    public loadSports() {
        const sports = jsonfile.readFileSync(__dirname + "/sports.json");
        console.log("sports = ", sports);
        return sports;
    }

    public editPlayer(editedPlayer: Player) {
        const player = this.players.find(player => player.playerId === editedPlayer.playerId);
        console.log("player = ", player);
        console.log("editedPlayer = ", editedPlayer);
        if (player) {
            const targetIndex = this.players.findIndex(player => player.playerId === editedPlayer.playerId);
            console.log("targetIndex = ", targetIndex);
            console.log(`this.players[${targetIndex}] = ${this.players[targetIndex]}`);
            this.players[targetIndex] = editedPlayer;
            console.log(`this.players[${targetIndex}] = ${this.players[targetIndex]}`);
            this.updatePlayers();
        } else {
            console.log("Invalid playerId");
            throw new Error("Invalid playerId");
        }
    }

    public updatePlayerImgSrc(playerId: string, imgSrc: string) {
        const currentPlayer = this.players.find(player => player.playerId === playerId);
        console.log("playerId = ", playerId);
        console.log("player = ", currentPlayer);
        if (currentPlayer) {
            console.log('imgSrc = ', imgSrc);
            currentPlayer.imgSrc = imgSrc;
            const targetIndex = this.players.findIndex(player => player.playerId === currentPlayer.playerId);
            console.log("targetIndex = ", targetIndex);
            console.log(`this.players[${targetIndex}] = ${this.players[targetIndex]}`);
            this.players[targetIndex] = currentPlayer;
            console.log(`this.players[${targetIndex}] = ${this.players[targetIndex]}`);
            this.updatePlayers();
        } else {
            console.log("Invalid playerId");
            throw new Error("Invalid playerId");
        }
    }

    public updatePlayerInfo(playerId: string, playerName: string, contacts: Contacts) {
        const currentPlayer = this.players.find(player => player.playerId === playerId);
        console.log("playerId = ", playerId);
        console.log("player = ", currentPlayer);
        if (currentPlayer) {
            console.log('playerName = ', playerName);
            currentPlayer.playerName = playerName;
            console.log('contacts = ', contacts);
            currentPlayer.contacts = contacts;
            const targetIndex = this.players.findIndex(player => player.playerId === currentPlayer.playerId);
            console.log("targetIndex = ", targetIndex);
            console.log(`this.players[${targetIndex}] = ${this.players[targetIndex]}`);
            this.players[targetIndex] = currentPlayer;
            console.log(`this.players[${targetIndex}] = ${this.players[targetIndex]}`);
            this.updatePlayers();
        } else {
            console.log("Invalid playerId");
            throw new Error("Invalid playerId");
        }
    }

    public updatePlayerTimeLocation(playerId: string, availability: Availability, location: Location) {
        const currentPlayer = this.players.find(player => player.playerId === playerId);
        console.log("playerId = ", playerId);
        console.log("currentPlayer = ", currentPlayer);
        if (currentPlayer) {
            console.log('availability = ', availability);
            console.log('location = ', location);
            currentPlayer.availability = availability;
            currentPlayer.location = location;
            // [CODE REVIEW] No need to findIndex and reassign back to the index. The `currentPlayer` can be modified directly and then save
            const targetIndex = this.players.findIndex(player => player.playerId === currentPlayer.playerId);
            console.log("targetIndex = ", targetIndex);
            console.log(`this.players[${targetIndex}] = ${this.players[targetIndex]}`);
            this.players[targetIndex] = currentPlayer;
            console.log(`this.players[${targetIndex}] = ${this.players[targetIndex]}`);
            this.updatePlayers();
        } else {
            console.log("Invalid playerId");
            throw new Error("Invalid playerId");
        }
    }

    public playerAddSport(playerId: string, newSport: Sport) {
        const currentPlayer = this.players.find(player => player.playerId === playerId);
        console.log("playerId = ", playerId);
        console.log("currentPlayer = ", currentPlayer);
        if (currentPlayer) {
            console.log("newSport = ", newSport);
            const targetSport = currentPlayer.sports.find(sport => sport.sport == newSport.sport);
            console.log("targetSport = ", targetSport);
            if (!targetSport) {
                currentPlayer.sports.push(newSport);
                console.log("currentPlayer.sports = ", currentPlayer.sports);
                const targetIndex = this.players.findIndex(player => player.playerId === currentPlayer.playerId);
                console.log("targetIndex = ", targetIndex);
                console.log(`this.players[${targetIndex}] = ${this.players[targetIndex]}`);
                this.players[targetIndex] = currentPlayer;
                console.log(`this.players[${targetIndex}] = ${this.players[targetIndex]}`);
                this.updatePlayers();
            } else {
                console.log(`Player has joined ${newSport.sport} already!`);
                throw new Error(`Player has joined ${newSport.sport} already!`);
            }
        } else {
            console.log("Invalid playerId");
            throw new Error("Invalid playerId");
        }
    }

    public updatePlayerProficiencyAfterMatch(player1Id: string, player2Id: string, sport:string, myNewElo: number, competitorNewElo: number, mySetNum:number, oppSetNum:number){
        const currentPlayer = this.players.find(player => player.playerId === player1Id);
        const opponentPlayer = this.players.find(player => player.playerId === player2Id);
        let mySportIndex = 0
        let oppSportIndex = 0
        
        if (currentPlayer && opponentPlayer) {
            // [CODE REVIEW] Use .find()
            for (let i = 0; i < currentPlayer.sports.length; i++){
                if (sport === currentPlayer.sports[i].sport){
                    mySportIndex = i
                } 
            }
            // [CODE REVIEW] May re-use `i` when it is not nested
            for (let j = 0; j < opponentPlayer.sports.length; j++){
                if (sport === opponentPlayer.sports[j].sport){
                    oppSportIndex = j
                }
            }
            if (mySetNum > oppSetNum){
                // Use += 1
                currentPlayer.sports[mySportIndex].record.win += 1;
                // [CODE REVIEW] should be oppSportIndex?
                opponentPlayer.sports[mySportIndex].record.lose += 1;
            }
                else {
                    currentPlayer.sports[mySportIndex].record.lose += 1;
                    opponentPlayer.sports[oppSportIndex].record.win += 1;
                    }
                
            currentPlayer.sports[mySportIndex].record.match += 1;
            currentPlayer.sports[mySportIndex].proficiency = myNewElo;
            opponentPlayer.sports[oppSportIndex].record.match += 1;
            opponentPlayer.sports[oppSportIndex].proficiency = competitorNewElo;

            const myTargetIndex = this.players.findIndex(player => player.playerId === currentPlayer.playerId);
            // console.log("myTargetIndex = ", myTargetIndex);
            // console.log(`this.players[${myTargetIndex}] = , ${this.players[myTargetIndex]}`);
            this.players[myTargetIndex] = currentPlayer;
            // console.log(`this.players[${myTargetIndex}] = , ${this.players[myTargetIndex]}`);
            const opponentTargetIndex = this.players.findIndex(player => player.playerId === opponentPlayer.playerId);
            this.players[opponentTargetIndex] = opponentPlayer;
            this.updatePlayers();
        
         } else {
            console.log("Invalid playerId");
                throw new Error("Invalid playerId");
         }
    }
}




// // // Test Codes
// const playerService1 = new PlayerService();
// // // const player = playerService.getPlayer("u00001");
// // // console.log("player = ", player);
// // // // playerService.addPlayer("u00003", "player3", 15, "M", 320, {match: 35, win: 10, lose: 25}, [22.3204, 114.1698], {}, {phone: 45678901, email: "user4@tecky.io"});
// // // playerService.addPlayer("u00004", "player4", 15, "F", 320, {match: 35, win: 10, lose: 25}, [22.3204, 114.1698], {}, {phone: 45678901, email: "user4@tecky.io"});
// // // const players = playerService.getPlayers();
// // // console.log("players = ", players);
// playerService1.updatePlayerProficiencyAfterMatch("u00001", "u00002", "tennis", 1, 2, "Third");