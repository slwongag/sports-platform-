import * as jsonfile from "jsonfile";


export interface Match {
    matchId: string;
    player1Id: string;
    player2Id: string;
    player1Name :string;
    player2Name: string;
    fromTime: string;
    toTime: string
    location: string;
    sports: string;
    remarks: string;
    status: string;
    player1SetNum: number,
    player2SetNum: number,
    formOfMatch: string
}

export class MatchInfoService {

    protected matches: Match[];

    constructor() {
        this.loadMatches();
    }

    public loadMatches() {
        this.matches = jsonfile.readFileSync(__dirname + "/matches.json");
        return this.matches;
    }

    private updateMatches() {
        jsonfile.writeFileSync(__dirname + "/matches.json", this.matches);
        console.log("this.matches = ", this.matches);
        return this.matches;
    }

    public findMatchByMatchId(matchId:string){
        const match = this.matches.find(match =>match.matchId === matchId);
        return match;
    }

    public findMatchesByPlayerId(playerId: string) {
        const match = this.matches.filter(match => match.player1Id === playerId || match.player2Id === playerId)
        return match;
    }

    public findMyMatchesByPlayerId(playerId:string){
        const match = this.findMatchesByPlayerId(playerId).filter(match =>match.player1Id === playerId )
        return match;
    }

    public findMyMatchesByStatus(playerId:string, status:string){
        const match = this.findMyMatchesByPlayerId(playerId).filter(match =>match.status === status )
        return match;
    }

    public findReceivedMatchesByPlayerId(playerId:string){
        const match = this.findMatchesByPlayerId(playerId).filter(match =>match.player2Id === playerId )
        return match;
    }

    public findReceivedMatchesByStatus(playerId:string, status:string){
        const match = this.findReceivedMatchesByPlayerId(playerId).filter(match =>match.status === status )
        return match;
    }

    public async createMatch(player1Id: string, player2Id: string, player1Name: string, player2Name:string, fromTime: string, toTime: string, location: string, sports: string, remarks: string) {

        const parsedMatchIds = this.matches.map((match) => parseInt(match.matchId.substring(1), 10));
        const max = Math.max(...parsedMatchIds, 0);
        // console.log("parsedUserIds = ", parsedUserIds);
        // console.log("max = ", max);
        const newMatch = {
            matchId: "m" + (max + 1).toString().padStart(5, "0"),
            player1Id: player1Id,
            player2Id: player2Id,
            player1Name: player1Name,
            player2Name: player2Name,
            fromTime: fromTime,
            toTime: toTime,
            location: location,
            sports: sports,
            remarks: remarks,
            status: "pending",
            player1SetNum: 0,
            player2SetNum: 0,
            formOfMatch: ""
        };
        this.matches.push(newMatch);
        await jsonfile.writeFile(__dirname + "/matches.json", this.matches);
        console.log(this.matches);
    }

    public updateMatchResult(matchId: string, formOfMatch: string, player1SetNum: number ,player2SetNum:number) {
        const currentMatch = this.matches.find(match => match.matchId === matchId);
        console.log("matchId = ", matchId);
        console.log("match = ", currentMatch);
        if (currentMatch) {
            console.log('match = ', matchId);
            currentMatch.formOfMatch = formOfMatch;
            console.log('formOfMatch = ', formOfMatch);
            currentMatch.player1SetNum = player1SetNum;
            currentMatch.player2SetNum = player2SetNum;
            currentMatch.status = "completed"
            const targetIndex = this.matches.findIndex(match => match.matchId === currentMatch.matchId);
            console.log("targetIndex = ", targetIndex);
            console.log(`this.matches[${targetIndex}] = , ${this.matches[targetIndex]}`);
            this.matches[targetIndex] = currentMatch;
            console.log(`this.matches[${targetIndex}] = , ${this.matches[targetIndex]}`);
            this.updateMatches();
        } else {
            console.log("Invalid matchId");
            throw new Error("Invalid matchId");
        }
    }

    public updateMatchStatus(matchId:string){
        const currentMatch = this.matches.find(match => match.matchId === matchId);
        console.log("matchId = ", matchId);
        console.log("match = ", currentMatch);
        if (currentMatch) {
            console.log('match = ', matchId);
            currentMatch.status = "Accepted";
            const targetIndex = this.matches.findIndex(match => match.matchId === currentMatch.matchId);
            console.log("targetIndex = ", targetIndex);
            console.log(`this.matches[${targetIndex}] = , ${this.matches[targetIndex]}`);
            this.matches[targetIndex] = currentMatch;
            console.log(`this.matches[${targetIndex}] = , ${this.matches[targetIndex]}`);
            this.updateMatches();
        } else {
            console.log("Invalid matchId");
            throw new Error("Invalid matchId");
        }
    }

    public updateMatchInfo(matchId:string, fromTime:string, toTime:string, location:string, remarks:string){
        const currentMatch = this.matches.find(match => match.matchId === matchId);
        console.log("matchId = ", matchId);
        console.log("match = ", currentMatch);
        if (currentMatch) {
            console.log('match = ', matchId);
            currentMatch.fromTime = fromTime;
            currentMatch.toTime = toTime;
            currentMatch.location = location;
            currentMatch.remarks = remarks

            const targetIndex = this.matches.findIndex(match => match.matchId === currentMatch.matchId);
            console.log("targetIndex = ", targetIndex);
            console.log(`this.matches[${targetIndex}] = , ${this.matches[targetIndex]}`);
            this.matches[targetIndex] = currentMatch;
            console.log(`this.matches[${targetIndex}] = , ${this.matches[targetIndex]}`);
            this.updateMatches();
        } else {
            console.log("Invalid matchId");
            throw new Error("Invalid matchId");
        }
    }

    public cancelMatchById(matchId:string, status:string){
        const currentMatch = this.matches.find(match =>match.matchId === matchId);
        if (currentMatch){
            currentMatch.status = status
            const targetIndex = this.matches.findIndex(match => match.matchId === currentMatch.matchId);
            this.matches[targetIndex] = currentMatch;
            this.updateMatches();
        }else {
            console.log("Invalid matchId");
            throw new Error("Invalid matchId");
        }
    }

    public acceptMatchById(matchId:string, status:string){
        const currentMatch = this.matches.find(match =>match.matchId === matchId);
        if (currentMatch){
            currentMatch.status = status
            const targetIndex = this.matches.findIndex(match => match.matchId === currentMatch.matchId);
            this.matches[targetIndex] = currentMatch;
            this.updateMatches();
        }else {
            console.log("Invalid matchId");
            throw new Error("Invalid matchId");
        }
    }
}


// test code
// const matchService = new MatchInfoService();

// console.log(matchService.findReceivedMatchesByPlayerId('u00001'))