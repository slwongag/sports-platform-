import * as jsonfile from "jsonfile";
import { hashPassword } from "../hash";

export interface User {
    userId: string, 
    username: string, 
    password: string
}

export class UserService {

    protected users: User[];

    constructor() {
        this.loadUsers();
    }

    private loadUsers() {
        this.users = jsonfile.readFileSync(__dirname + "/users.json");
        return this.users;
    }
    
    private updateUsers() {
        jsonfile.writeFileSync(__dirname + "/users.json", this.users);
        this.users = this.loadUsers();
        console.log("this.users = ", this.users);
        return this.users;
    }

    public getUsers() {
        return this.users;
    }

    public findUser(username: string, password: string) {
        const user = this.users.find(user => user.username === username && user.password === password);
        return user;
    }

    public findUserByUserId(userId: string) {
        const user = this.users.find(user => user.userId === userId);
        return user;
    }

    public findUserByUsername(username: string) {
        const user = this.users.find(user => user.username === username);
        console.log("user = ", user);
        return user;
    }
    
    public async addUser(username: string, password: string) {
        if (this.findUserByUsername(username)) {
            throw new Error("username already taken");
        } else {
            const parsedUserIds = this.users.map(user => parseInt(user.userId.substring(1), 10));
            const max = Math.max(...parsedUserIds, 0);
            // console.log("parsedUserIds = ", parsedUserIds);
            // console.log("max = ", max);
            const hash = await hashPassword(password);
            const newUser = {userId: "u" + (max + 1).toString().padStart(5, "0"), username: username, password: hash};
            console.log("newUser = ", newUser);
            this.users.push(newUser);
            this.updateUsers();
        }
    }
    
}

// // Test Codes
// const userService = new UserService();
// // userService.addUser("user1", "111");
// userService.addUser("user4", "444");
// userService.findUserByUsername("user1");
// userService.findUserByUsername("user5");