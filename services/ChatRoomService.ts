import * as jsonfile from "jsonfile";

export interface Content {
    conversation: { speaker: string, listener: string, saying: string }
}
export interface ChatRoom {
    chatRoomId: string;
    content: Content[]
    player1Id: string;
    player2Id: string;
}

export class ChatRoomService {
    protected chatRooms: ChatRoom[];

    constructor() {
        this.loadChatRooms();
    }

    public loadChatRooms() {
        this.chatRooms = jsonfile.readFileSync(__dirname + "/chatRooms.json");
        return this.chatRooms;
    }

    public loadPlayers(){
        let players = jsonfile.readFileSync(__dirname + "/players.json")
        return players
    }

    public updateChatRoom() {
        jsonfile.writeFileSync(__dirname + "/chatRooms.json", this.chatRooms);
    }

    public findChatRoomByChatRoomId(chatRoomId: string) {
        const chatRoom = this.chatRooms.find(chatRoom => chatRoom.chatRoomId == chatRoomId);
        return chatRoom;
    }

    public findChatRoomByPlayerId(playerId: string) {
        const chatRoom = this.chatRooms.filter(chatRoom => chatRoom.player1Id === playerId || chatRoom.player2Id === playerId)
        return chatRoom;
    }

    public findChatRoomByTwoPlayerId(player1Id: string, player2Id: string) {
        for (let chatRoom of this.chatRooms) {
            if ((player1Id == chatRoom.player1Id && player2Id == chatRoom.player2Id) || (player1Id == chatRoom.player2Id && player2Id == chatRoom.player1Id)) {
                return chatRoom;
            }
        }
        return;
    }

    public checkIfTwoPlayersHaveChatRoom(player1Id: string, player2Id: string) {
        for (let chatRoom of this.chatRooms) {
            if ((player1Id == chatRoom.player1Id && player2Id == chatRoom.player2Id) || (player1Id == chatRoom.player2Id && player2Id == chatRoom.player1Id)) {
                return true;
            }
        }
        return false;
    }

    public findChatRoomContent(chatRoom: ChatRoom) {
        return chatRoom.content;
    }

    public createChatRoom(player1Id:string,player2Id:string) {
        if (this.checkIfThePlayerExist(player2Id)==false){
            throw new Error("the player does not exist");
        } else if (this.checkIfTwoPlayersHaveChatRoom(player1Id, player2Id) == false && this.checkIfThePlayerExist(player2Id)&&player1Id!=player2Id) {
            const parsedMatchIds = this.chatRooms.map((chatRoom) => parseInt(chatRoom.chatRoomId.substring(1), 10));
            const max = Math.max(...parsedMatchIds, 0);
            // console.log("parsedUserIds = ", parsedUserIds);
            // console.log("max = ", max);
            const newChatRoom = {
                chatRoomId: "m" + (max + 1).toString().padStart(5, "0"),
                player1Id: player1Id,
                player2Id: player2Id,
                content: []
            };
            this.chatRooms.push(newChatRoom);
            console.log(this.chatRooms);
            this.updateChatRoom();
            return newChatRoom.chatRoomId;
        }
        else {
            // throw new Error("chatRoom already exists");
            console.log("chatRoom already exists");
            const existingChat = this.findChatRoomByTwoPlayerId(player1Id, player2Id);
            if (existingChat) {
                return existingChat.chatRoomId;
            } else {
                throw new Error("something went terribly wrong!");
            }
        }

    }

    public returnTheChatRoomOfTheMatch(player1Id:string,player2Id:string) {
        return this.findChatRoomByTwoPlayerId(player1Id, player2Id)
    }

    public addContent(conversation: string, speaker: string, listener: string) {
        let chatRoom: ChatRoom | undefined = this.findChatRoomByTwoPlayerId(speaker, listener)
        console.log(chatRoom)
        if (chatRoom != undefined) {
            let subContent: Content = { conversation: { speaker: speaker, listener: listener, saying: conversation } }
            chatRoom.content.push(subContent)
            this.updateChatRoom();
        }
        else {
            throw new Error("chatRoom does not exist");
        }
    }

    public getChatRoomId(player1Id:string,player2Id:string){
        for (let chatRoom of this.chatRooms){
            if ((chatRoom.player1Id==player1Id && chatRoom.player2Id==player2Id)||(chatRoom.player2Id==player1Id&&chatRoom.player1Id==player2Id)){
                return chatRoom.chatRoomId
            }
        }
        return;
    }

    public checkIfThePlayerExist(firend:string){
        let players = this.loadPlayers();
        for (let player of players){
            if (player.playerId==firend){
                return true;
            }
        }
        return false;
    }
}

let chatRoomService = new ChatRoomService()
console.log(chatRoomService.checkIfThePlayerExist('player'))