window.onload = async () => {
    player = await loadPlayer(true);
    const url = location.href;
    console.log("url = ", url);
    const params = new URLSearchParams(url);
    console.log("params = ", params);
    const targetPlayerId = params.get("playerId");
    console.log("targetPlayerId = ", targetPlayerId);
    loadPlayerDetails();
    await loadSports();
}

let player;

async function loadPlayer(overwrite = false) {

    // console.log("overwrite === true: ", overwrite === true);
    if (overwrite === true) {

        // do not check if player already exists in local storage 
        const res = await fetch("/protected/player/info", { method: "GET" });
        const playerData = await res.json();
        // console.log("playerData = ", playerData);
        localStorage.setItem("player", JSON.stringify(playerData));

    } else {

        // check if player already exists in local storage first 
        // console.log(`localStorage.getItem("player") = `, localStorage.getItem("player"));
        // console.log(`localStorage.getItem("player") == null: `, localStorage.getItem("player") == null);

        if (localStorage.getItem("player") == null) {
            const res = await fetch("/protected/player/info", { method: "GET" });
            const playerData = await res.json();
            // console.log("playerData = ", playerData);
            localStorage.setItem("player", JSON.stringify(playerData));
        }

    }

    // console.log(`JSON.parse(localStorage.getItem("player")) = `, JSON.parse(localStorage.getItem("player")));
    // console.log(`JSON.parse(localStorage.getItem("player")).playerId = `, JSON.parse(localStorage.getItem("player")).playerId);

    return JSON.parse(localStorage.getItem("player"));

}

// load player details
function loadPlayerDetails() {

    const profilePicSrc = player.imgSrc;
    console.log("profilePicSrc = ", profilePicSrc);
    playerPhoto.setAttribute("src", profilePicSrc);

    playerName.value = player.playerName;
    gender.innerHTML = "Gender: " + player.gender;
    age.innerHTML = "Age: " + player.age;

    email.value = player.contacts.email;
    phone.value = player.contacts.phone;

    for (let timeSlot of timeSlots) {
        const timeSlotIdNum = timeSlot.id.substring(timeSlot.id.length - 2);
        // console.log("timeSlotIdNum = ", timeSlotIdNum);
        if (player.availability[timeSlotIdNum[0]][timeSlotIdNum[1]] === 1) {
            timeSlot.checked = true;
        } else {
            timeSlot.checked = false;
        }
    }

    markerLocation();
    const lat = document.querySelector("#lat").value;
    const lng = document.querySelector("#lng").value;

}

// load sports tabs and panels
async function loadSports() {
    const res = await fetch("/protected/player/sports", { method: "GET" });
    const sports = await res.json();
    // console.log("sports = ", sports);

    const sportOptions = document.querySelector("#sports-options");
    let tabsHtml = "";
    let contentsHtml = "";

    for (let sport of sports) {
        const sportName = sport.split(" ").map(word => word[0].toUpperCase() + word.slice(1)).join(" ");
        // console.log("sportName = ", sportName);
        const sportId = sport.replace(/\s/g, "-");
        // const sportId = sport.split(" ").join("-");
        // console.log("sportId = ", sportId);

        const sportData = player.sports.find(sports => sports.sport === sport);
        // console.log("sportData = ", sportData);

        tabsHtml += `
            <a class="nav-link sportPill" id="${sportId}-tab" data-toggle="pill" href="#${sportId}" role="tab" aria-controls="${sportId}" aria-selected="false">${sportName}</a>            
        `;

        if (sportData) {

            // render sportPie
            const sportPieHtml = `
                <div class="sportPie" id="${sportId}-pie">
                    
                </div>
            `;
            
            // calculate winRate
            let winRateResult = Math.round(100 * sportData.record.win / sportData.record.match);
            console.log("winRateResult = ", winRateResult);
            if (isNaN(winRateResult)) {
                winRateResult = "N/A";
            } else {
                winRateResult = winRateResult + "%";
            }
            
            // specify pieData
            const pieDataHtml = `
                <div class="pieData" id="${sportId}-data">
                    <div class="pieDataContent winRate ${sportId}-rate" id="${sportId}-rate">${winRateResult}</div>
                    <div class="pieDataContent dataRecord dataWin">Win: ${sportData.record.win}</div>
                    <div class="pieDataContent dataRecord dataLose">Lose: ${sportData.record.lose}</div>
                    <div class="pieDataContent dataRecord dataTotal">Total: ${sportData.record.match}</div>
                </div>
            `;

            // specify sportStats
            const sportStatsHtml = `
                <div class="col-sm-6 sportStats ${sportId}-stats">
                    <div class="sportRank" id="${sportId}-rank">
                        
                    </div>
                    <div>Proficiency: ${sportData.proficiency}</div>
                </div>
            `;

            // specify sportContentHtml
            const sportContentHtml = `
                <div class="tab-pane fade sportContent" id="${sportId}" role="tabpanel" aria-labelledby="${sportId}-tab">
                    <h5>${sportName}</h5>
                    <div class="row sportStatsWrap">
                        <div class="col-sm-6 sportPieWrap">
            ` + sportPieHtml + pieDataHtml + `
                        </div>
            ` + sportStatsHtml + `
                    </div>
                    <div class="matchRecordWrap">
                        <button class="matchRecord themeButton sportButton ${sportId}-record" id="${sportId}-record">View Match Records</button>
                    </div>
                </div>
            `;

            // add to contentHtml
            contentsHtml += sportContentHtml;

        } else {
            contentsHtml += `
                <div class="tab-pane fade sportContent" id="${sportId}" role="tabpanel" aria-labelledby="${sportId}-tab">
                    <p>${sportName}</p>
                    <div class="noSportStats no-${sportId}">
                        <h6>You have not joined ${sportName} yet!</h6>
                    </div>
                    <div class="joinSportWrap">
                        <button class="joinSport themeButton sportButton join-${sportId}" id="join-${sportId}">Join ${sportName} Now!</button>
                    </div>
                </div>
            `;
        }

    }

    sportTabs.innerHTML = tabsHtml;
    sportContentWrap.innerHTML = contentsHtml;

    const sportPills = document.querySelectorAll(".sportPill");
    const sportContents = document.querySelectorAll(".sportContent");
    // console.log("sportPills[0].id = ", sportPills[0].id);
    // console.log("sportContents[0].id = ", sportContents[0].id);

    sportPills[0].classList.add("active");
    sportPills[0].setAttribute("aria-selected", "true");
    sportContents[0].classList.add("active");
    sportContents[0].classList.add("show");
    
    await retrievePlayerRanks();
    
    renderPieForPlayerStats();
    
    // add join sports functions
    const joinSports = document.querySelectorAll(".joinSport");
    for (let joinSport of joinSports) {
        joinSport.addEventListener("click", async (event) => {
            const sportIdForJoin = joinSport.id.replace('join-', "");
            const sportForJoin = sportIdForJoin.replace(/-/g, " ");
            console.log("joinSport.id = ", joinSport.id);
            console.log("sportIdForJoin = ", sportIdForJoin);
            console.log("sportForJoin = ", sportForJoin);
            await fetch("/protected/player/joinSport", {
                method: "POST", 
                headers: {
                    "Content-Type": "application/json; charset=utf-8"
                }, 
                body: JSON.stringify({sport: sportForJoin})
            });
            await window.onload();
        })
    }

    // add redirection to view match records
    const matchRecords = document.querySelectorAll(".matchRecord");
    for (let matchRecord of matchRecords) {
        matchRecord.addEventListener("click", async (event) => {
            location.href = "/protected/record";
        });
    }

}

// retrieve player's ranks in sports
async function retrievePlayerRanks() {
    
    const sportRanks = document.querySelectorAll(".sportRank");
    for (let sportRank of sportRanks) {
    
        // make sportId for rank
        const sportIdForRank = sportRank.id.replace('-rank', "");
        // console.log("sportIdForRank = ", sportIdForRank);
    
        // fetch ranked players list
        const res = await fetch(`/leaderBoard/${sportIdForRank}`, { method: "GET" });
        const rankedPlayers = await res.json();
        // console.log("rankedPlayers = ", rankedPlayers);
    
        // find player's current rank
        const currentRank = rankedPlayers.findIndex(rankedPlayer => rankedPlayer.playerId === player.playerId) + 1;
        // console.log("currentRank = ", currentRank);
    
        sportRank.innerHTML = `Current Rank: <span class="playerCurrentRank">#${currentRank}</span>`;
    
    }

}

// render pie chart for player's stats 
function renderPieForPlayerStats() {

    const sportPies = document.querySelectorAll(".sportPie");
    for (let sportPie of sportPies) {
        const sportIdForPie = sportPie.id.replace('-pie', "");
        // console.log("sportIdForPie = ", sportIdForPie);
    
        const pieContentsHtml = document.querySelector(`#${sportIdForPie}-data`).innerHTML;
        sportPie.innerHTML = `
            <!-- CSS Circular Sectors -->
            <!-- https://stackoverflow.com/questions/11487557/how-can-i-make-a-circular-sector-using-css/11497542 -->
            <div class="menu" id="${sportIdForPie}-menu">
    
                <div class="slice ${sportIdForPie}-slice sliceWin sliceWin1 slice-00" id="${sportIdForPie}-slice-00"></div>
                <div class="slice ${sportIdForPie}-slice sliceWin sliceWin2 slice-01" id="${sportIdForPie}-slice-01"></div>
                <div class="slice ${sportIdForPie}-slice sliceWin sliceWin3 slice-02" id="${sportIdForPie}-slice-02"></div>
                <div class="slice ${sportIdForPie}-slice sliceLose sliceLose1 slice-10" id="${sportIdForPie}-slice-10"></div>
                <div class="slice ${sportIdForPie}-slice sliceLose sliceLose2 slice-11" id="${sportIdForPie}-slice-11"></div>
                <div class="slice ${sportIdForPie}-slice sliceLose sliceLose3 slice-12" id="${sportIdForPie}-slice-12"></div>
                <div class="pieContents" id="${sportIdForPie}-contents">
                ` + pieContentsHtml + `
                </div>
    
            </div>
        `;
    
        const slices = document.querySelectorAll(`.${sportIdForPie}-slice`);
    
        const winRateDiv = document.querySelector(`#${sportIdForPie}-rate`);
        let winRateForPie;
        console.log("winRateDiv.innerHTML = ", winRateDiv.innerHTML);
        if (winRateDiv.innerHTML == 'N/A') {
            winRateForPie = 50;
        } else {
            winRateForPie = parseInt(winRateDiv.innerHTML.slice(0, -1), 10);
        }
        console.log("winRateForPie = ", winRateForPie);
    
        const winDeg = Math.round(winRateForPie * 360 / 100);
        console.log("winDeg = ", winDeg);
    
        let degArray;
        if (winDeg <= 120) {
            // winDeg: 0deg - 120deg
            degArray = [
                [winDeg, 0, 0], 
                [120, 120, 120 - winDeg]
            ];
        } else if (winDeg > 120 && winDeg <= 240) {
            // winDeg: 121deg - 240deg
            degArray = [
                [120, winDeg - 120, 0], 
                [120, 240 - winDeg, 0]
            ];
        } else {
            // winDeg: 241deg - 360deg
            degArray = [
                [120, 120, winDeg - 240], 
                [360 - winDeg, 0, 0]
            ];
        }
        console.log("degArray = ", degArray);
    
        let rotateDeg = 0;
        for (let slice of slices) {
    
            const sliceIdString = slice.id.slice(-2);
            // console.log("sliceIdString = ", sliceIdString);
    
            const assignDeg = degArray[parseInt(sliceIdString[0], 10)][parseInt(sliceIdString[1], 10)];
            // console.log("assignDeg = ", assignDeg);
    
            const sliceTransform = `
                transform: rotate(${rotateDeg}deg) skewY(${assignDeg - 90}deg);
                -moz-transform: rotate(${rotateDeg}deg) skewY(${assignDeg - 90}deg);
                -webkit-transform: rotate(${rotateDeg}deg) skewY(${assignDeg - 90}deg);
            `;
            slice.setAttribute("style", sliceTransform);
    
            rotateDeg += assignDeg;
        }
    
    }

}




var map; //Will contain map object.
var marker = false; ////Has the user plotted their location marker? 

//Function called to initialize / create the map.
//This is called when the page has loaded.
async function initMap() {
    // Load player's location
    const res = await fetch("/protected/player/info", { method: "GET" });
    const player = await res.json();
    // console.log("player = ", player);
    // console.log("player.location = ", player.location);

    //The center location of our map.
    var centerOfMap = new google.maps.LatLng(player.location[0], player.location[1]);

    //Map options.
    var options = {
        center: centerOfMap, //Set center.
        zoom: 12 //The zoom value.
    };

    //Create the map object.
    map = new google.maps.Map(document.getElementById('map'), options);

    const playLatLng = new google.maps.LatLng({ lat: player.location[0], lng: player.location[1] });
    console.log("playLatLng = ", playLatLng);

    // Create marker and set initial location to player's location
    marker = new google.maps.Marker({
        position: playLatLng,
        map: map,
        draggable: true //make it draggable
    });
    console.log("marker = ", marker);

    //Listen for drag events!
    google.maps.event.addListener(marker, 'dragend', function (event) {
        markerLocation();
    });

    //Listen for any clicks on the map.
    google.maps.event.addListener(map, 'click', function (event) {
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        //If the marker hasn't been added.
        if (marker === false) {
            console.log("hello")
            //Create the marker.
            marker = new google.maps.Marker({
                position: clickedLocation,
                map: map,
                draggable: true //make it draggable
            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function (event) {
                markerLocation();
            });
        } else {
            //Marker has already been added, so just change its location.
            console.log("hi")
            marker.setPosition(clickedLocation);
            console.log(clickedLocation)
        }
        //Get the marker's location.
        markerLocation();
    });
}

//This function will get the marker's current location and then add the lat/long
//values to our textfields so that we can save the location.
function markerLocation() {
    //Get location.
    var currentLocation = marker.getPosition();
    //Add lat and lng values to a field that we can save.
    document.getElementById('lat').value = currentLocation.lat(); //latitude
    document.getElementById('lng').value = currentLocation.lng(); //longitude
}


//Load the map when the page has finished loading.
google.maps.event.addDomListener(window, 'load', initMap());

function codeAddress() {
    geocoder = new google.maps.Geocoder();
    var address = document.getElementById('address').value;
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == 'OK') {
            map.setCenter(results[0].geometry.location);
            if (marker === false) {
                marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    draggable: true
                });
            }
            else {
                marker.setPosition(results[0].geometry.location);
            }
            console.log(results[0].geometry.location)
            var currentLocation = marker.getPosition();
            document.getElementById('lat').value = currentLocation.lat(); //latitude
            document.getElementById('lng').value = currentLocation.lng(); //longitude
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function geoFindMe() {
    var output = document.getElementById("out");

    if (!navigator.geolocation) {
        output.innerHTML = "<p>Geolocation is not supported by your browser</p>";
        return;
    }

    function success(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;

        document.getElementById('lat').value = position.coords.latitude; //latitude
        document.getElementById('lng').value = position.coords.longitude; //longitude
        pos = new google.maps.LatLng({ lat: latitude, lng: longitude });
        map.setCenter(pos);
        if (marker === false) {
            marker = new google.maps.Marker({
                position: pos,
                map: map,
                draggable: true
            });
        }
        else {
            marker.setPosition(pos);
        }
    }

    function error() {
        output.innerHTML = "Unable to retrieve your location";
    };

    navigator.geolocation.getCurrentPosition(success, error);
}












const profilePicForm = document.querySelector("#profilePicForm");

const playerPhoto = document.querySelector("#playerPhoto");
const profilePicFile = document.querySelector("#profilePicFile");
const uploadInfo = document.querySelector("#uploadInfo");

const playerInfoForm = document.querySelector("#playerInfoForm");

const editableInputs = document.querySelectorAll(".editableInput");

const playerName = document.querySelector("#playerNameInput");
const gender = document.querySelector("#genderInput");
const age = document.querySelector("#ageInput");
const email = document.querySelector("#emailInput");
const phone = document.querySelector("#phoneInput");
const playerInfoEditSave = document.querySelector("#playerInfoEditSave");

const sportTabs = document.querySelector("#sportTabs")
const sportContentWrap = document.querySelector("#sportContents")

const timeLocationForm = document.querySelector("#timeLocationForm");

const timeSlots = document.querySelectorAll(".time-slot");


// Select Profile Pic
profilePicFile.addEventListener("change", (event) => {

    if (profilePicFile.files[0]) {
        const uploadFileName = profilePicFile.files[0].name;
        console.log("uploadFileName = ", uploadFileName);
        let html = uploadFileName;
        console.log("html = ", html);
        uploadInfo.innerHTML = html;
    }

})

// Profile Pic Upload (imgSrc)
profilePicForm.addEventListener("submit", async (event) => {

    event.preventDefault();
    console.log("event.currentTarget.id = ", event.currentTarget.id);

    let html;
    if (profilePicFile.files[0]) {

        console.log("profilePicFile.files = ", profilePicFile.files);

        const profilePicFormData = new FormData(profilePicForm);

        const res = await fetch("/protected/player/uploadProfilePic", {
            method: "POST",
            body: profilePicFormData
        });
        const upload = await res.json();
        const result = upload.result;
        console.log("upload = ", upload);
        console.log("result = ", result);

        html = `
            <div class="icon">
                <i class="fas fa-check"></i>
            </div>
            ${result}
        `;

        profilePicFile.value = "";
        console.log("profilePicFile.files = ", profilePicFile.files);

    } else {
        // console.log("profilePicFile.files = ", profilePicFile.files);
        html = `
            <div class="icon icon-alert">
                <i class="fas fa-exclamation"></i>
            </div>
            Please select a file!
        `;
    }

    console.log("html = ", html);
    uploadInfo.innerHTML = html;

    player = await loadPlayer(true);
    loadPlayerDetails();

})


// Edit Player Info (playerName & contacts)
playerInfoForm.addEventListener("submit", async (event) => {

    event.preventDefault();
    console.log("event.currentTarget.id = ", event.currentTarget.id);

    if (playerInfoEditSave.value == 'Edit') {

        // edit profile protocol
        console.log("edit profile protocol");
        editPlayerInfoForm();

    } else if (playerInfoEditSave.value == 'Save') {

        // save profile protocol
        console.log("save profile protocol");
        await savePlayerInfoForm();

    } else {
        
        // something went wrong
        console.log("something went wrong!");
    }

});

function editPlayerInfoForm() {

    for (let editableInput of editableInputs) {
        editableInput.classList.remove("readOnly");
        editableInput.removeAttribute("readonly");
    }
    playerInfoEditSave.value = "Save";

}

async function savePlayerInfoForm() {

    for (let editableInput of editableInputs) {
        editableInput.classList.add("readOnly");
        editableInput.setAttribute("readonly", "");
    }

    let contacts = {
        phone: phone.value,
        email: email.value
    }

    editedPlayerName = playerName.value;
    editedContacts = contacts;

    const res = await fetch("/protected/player/updatePlayerInfo", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({
            editedPlayerName: editedPlayerName,
            editedContacts: editedContacts
        })
    });

    const edit = await res.json();
    console.log(edit.result);

    playerInfoEditSave.value = "Edit";
    player = await loadPlayer(true);
    loadPlayerDetails();

}

// Update time & location (availability & location)
timeLocationForm.addEventListener("submit", async (event) => {

    event.preventDefault();
    console.log("event.currentTarget.id = ", event.currentTarget.id);

    let availability = [
        [null, null],
        [null, null],
        [null, null],
        [null, null],
        [null, null],
        [null, null],
        [null, null]
    ];
    console.log("timeSlots = ", timeSlots);
    for (let timeSlot of timeSlots) {
        const timeSlotId = timeSlot.id;
        console.log("timeSlotId = ", timeSlotId);
        const timeSlotIdValues = timeSlot.id.substring(timeSlot.id.length - 2);
        console.log("timeSlotIdValues = ", timeSlotIdValues);
        availability[parseInt(timeSlotIdValues[0], 10)][parseInt(timeSlotIdValues[1], 10)] = timeSlot.checked ? 1 : 0;
        console.log("timeSlot.checked = ", timeSlot.checked);
    }
    console.log("availability = ", availability);

    console.log(timeLocationForm.lat.value);
    console.log(timeLocationForm.lng.value);
    const x = parseFloat(timeLocationForm.lat.value);
    const y = parseFloat(timeLocationForm.lng.value);
    console.log("x = ", x);
    console.log("y = ", y);

    const res = await fetch("/protected/player/updateTimeLocation", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({
            availability: availability,
            latitude: x,
            longitude: y
        })
    });

    player = await loadPlayer(true);
    loadPlayerDetails();

})

// Load Rank Data
