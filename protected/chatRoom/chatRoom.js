window.onload = async () => {
    player = await loadPlayer();
    // players = await loadPlayers();
    playersSocial = await loadPlayersSocial();
    myId = player.playerId;
    // myName = await translatePlayerIdToPlayerName(myId);
    myName = player.playerName;
    initializeMe();
    await loadContact();
}
let myName;
let myId
let player;
// let players;
let playersSocial;


function initializeMe() {
    // document.querySelector(".invitation input.me").value = myName;
    document.querySelector(".invitation input.me").value = myId;
}

function findChatRoomIdId(myId, friendsId, chatRooms) {
    for (let chatRoom of chatRooms) {
        if ((chatRoom.player1Id == myId && chatRoom.player2Id == friendsId) || (chatRoom.player2Id == myId && chatRoom.player1Id == friendsId)) {
            return chatRoom.chatRoomId;
        }
    }
    return;
}

function clearDialog() {
    const dialog = document.querySelector("#dialog");
    let html = "";
    dialog.innerHTML = html;
}

function updateDialogAfterPressingConnect() {
    // const contactList = document.querySelectorAll('.list-group button')
    const contactList = document.querySelectorAll('.chatRoomDoor')
    for (let contact of contactList) {
        // if (contact.innerHTML == chatRoomForm.friends.value) {
        if (contact.dataset.id == chatRoomForm.dataset.id) {
            console.log('byebye');
            contact.click();
            return;
        }
    }
}
async function loadPlayer(overwrite = false) {
    console.log("overwrite === true: ", overwrite === true);
    if (overwrite === true) {

        // do not check if player already exists in local storage 
        const res = await fetch("/protected/player/info", { method: "GET" });
        const playerData = await res.json();
        console.log("playerData = ", playerData);
        localStorage.setItem("player", JSON.stringify(playerData));

    } else {

        // check if player already exists in local storage first 
        // console.log(`localStorage.getItem("player") = `, localStorage.getItem("player"));
        // console.log(`localStorage.getItem("player") == null: `, localStorage.getItem("player") == null);

        if (localStorage.getItem("player") == null) {
            const res = await fetch("/protected/player/info", { method: "GET" });
            const playerData = await res.json();
            // console.log("playerData = ", playerData);
            localStorage.setItem("player", JSON.stringify(playerData));
        }

    }

    // console.log(`JSON.parse(localStorage.getItem("player")) = `, JSON.parse(localStorage.getItem("player")));
    // console.log(`JSON.parse(localStorage.getItem("player")).playerId = `, JSON.parse(localStorage.getItem("player")).playerId);

    return JSON.parse(localStorage.getItem("player"));

}

async function loadConversation(chatRoomId) {
    // // const res = await fetch("/protected/chatRoom/chatRooms", { method: "GET" });
    // // const chatRooms = await res.json();
    // const chatRooms = await loadChatRooms();
    // console.log("chatRooms = ", chatRooms);
    // const dialog = document.querySelector("#dialog");
    // // const dialog1 = document.querySelector("#dialog");
    // // const dialog2 = document.querySelector("#dialog2");
    // let dialogHtml = "";
    // // let html1 = "";
    // // let html2 = "";
    // for (let chatRoom of chatRooms) {
    //     // if (chatRoom.player1Id ==  translatePlayerNameToPlayerId(myName) || chatRoom.player2Id == translatePlayerNameToPlayerId(myName)) {
    //     if (chatRoom.player1Id == myId || chatRoom.player2Id ==myId) {
    //         if (chatRoom.chatRoomId == chatRoomId) {
    //             for (let content of chatRoom.content) {
    //                 if (content.conversation.speaker == myId) {
    //                     dialogHtml += `
    //                         <div class="inside speech-bubble theme-color-bg-3 myDialog">
    //                             <p>${content.conversation.saying}</p>
    //                         </div>
    //                     `;
    //                     // html1 += `
    //                     //     <div class="inside">
    //                     //         <p>${content.conversation.saying}</p>
    //                     //     </div>
    //                     // `;
    //                     // html2 += `
    //                     //     <div class="inside">
    //                     //         <p><br></p>
    //                     //     </div>
    //                     // `;
    //                 }
    //                 else if (content.conversation.listener == myId) {
    //                     dialogHtml += `
    //                         <div class="inside speech-bubble theme-color-bg-3 friendsDialog">
    //                             <p>${content.conversation.saying}</p>
    //                         </div>
    //                     `;
    //                     // html2 += `
    //                     //     <div class="inside">
    //                     //         <p>${content.conversation.saying}</p>
    //                     //     </div>
    //                     // `;
    //                     // html1 += `
    //                     //     <div class="inside">
    //                     //         <p><br></p>
    //                     //     </div>
    //                     // `;
    //                 }
    //             }
    //         }

    //     }
    // }
    // dialog.innerHTML = dialogHtml;
    // // dialog1.innerHTML = html2;
    // // dialog2.innerHTML = html1;
    // const convoScroll = document.querySelector(".convoScroll");
    // convoScroll.scrollTop = convoScroll.scrollHeight;

    const chatRooms = await loadChatRooms();
    console.log("chatRooms = ", chatRooms);
    const dialog = document.querySelector("#dialog");
    let dialogHtml = "";
    const chatRoom = chatRooms.find(chatRoom => chatRoom.chatRoomId === chatRoomId);
    console.log("chatRoom = ", chatRoom);
    for (let content of chatRoom.content) {
        if (content.conversation.speaker == myId) {
            dialogHtml += `
                <div class="inside speech-bubble theme-color-bg-3 myDialog">
                    <p>${content.conversation.saying}</p>
                </div>
            `;
        }
        else if (content.conversation.listener == myId) {
            dialogHtml += `
                <div class="inside speech-bubble theme-color-bg-3 friendsDialog">
                    <p>${content.conversation.saying}</p>
                </div>
            `;
        }
    }
    dialog.innerHTML = dialogHtml;
    const convoScroll = document.querySelector(".convoScroll");
    convoScroll.scrollTop = convoScroll.scrollHeight;

}

async function loadContact() {
    // const res = await fetch("/protected/chatRoom/chatRooms", { method: "GET" });
    // const chatRooms = await res.json();
    const chatRooms = await loadChatRooms();
    const contact = document.querySelector('#contact-list')
    let html = "";
    for (let chatRoom of chatRooms) {
        // if (chatRoom.player1Id == myId || chatRoom.player2Id == myId) {
        //     if (chatRoom.player1Id == myId) {
        //         html += `<button type="button" class="list-group-item list-group-item-action chatRoomDoor">${await translatePlayerIdToPlayerName(chatRoom.player2Id)}</button>`;
        //     }
        //     else {
        //         html += `<button type="button" class="list-group-item list-group-item-action chatRoomDoor">${await translatePlayerIdToPlayerName(chatRoom.player1Id)}</button>`;
        //     }
        // }
        if (chatRoom.player1Id == myId || chatRoom.player2Id == myId) {
            const friendsId = chatRoom.player1Id == myId ? chatRoom.player2Id : chatRoom.player1Id;
            const friendsSocial = playersSocial.find(playerSocial => playerSocial.playerId === friendsId);
            html += `<button type="button" class="list-group-item list-group-item-action chatRoomDoor" data-id="${chatRoom.chatRoomId}">${friendsSocial.playerName}</button>`;
        }
        contact.innerHTML = html;
        await action();
    }
    console.log('bye');
}

// async function loadPlayers(){
//     const res = await fetch("/protected/player/players", { method: "GET" });
//     const players = await res.json();
//     return players;
// }

async function loadPlayersSocial() {
    const res = await fetch("/protected/player/playersSocial", { method: "GET" });
    const playersSocial = await res.json();
    return playersSocial;
}

async function loadChatRooms() {
    const res = await fetch("/protected/chatRoom/chatRooms", { method: "GET" });
    const chatRooms = await res.json();
    return chatRooms;
}

// function translatePlayerNameToPlayerId(playerName) {
//     // for (let player of players) {
//     for (let player of playersSocial) {
//         if (player.playerName == playerName) {
//             return player.playerId;
//         }
//     }
// }

// function translatePlayerIdToPlayerName(playerId) {
//     // for (let player of players) {
//     for (let player of playersSocial) {
//         if (player.playerId == playerId) {
//             return player.playerName;
//         }
//     }
// }

async function action() {
    // const res = await fetch("/protected/chatRoom/chatRooms", { method: "GET" });
    // const chatRooms = await res.json();
    const chatRooms = await loadChatRooms();
    const friends = document.querySelector('#chatRoomForm #friends');
    // const contactList = document.querySelectorAll('.list-group button');
    const contactList = document.querySelectorAll('.chatRoomDoor');
    const enroll = document.querySelector('.join #room-number');
    const roomNumber = document.querySelector('.send .room');
    for (let contact of contactList) {
        contact.addEventListener("click", async () => {
            // friends.value = contact.innerHTML;
            // const friendId = translatePlayerNameToPlayerId(contact.innerHTML);
            // enroll.value = findChatRoomIdId(myId, friendId, chatRooms);
            // roomNumber.value = findChatRoomIdId(myId, friendId, chatRooms);
            // await loadConversation(findChatRoomIdId(myId, friendId, chatRooms));
            const chatId = contact.dataset.id;
            const chat = chatRooms.find(chatRoom => chatRoom.chatRoomId === chatId);
            const friendsId = chat.player1Id === myId ? chat.player2Id : chat.player1Id;
            friends.value = friendsId;
            const playerSocial = playersSocial.find(playerSocial => playerSocial.playerId === friendsId);

            const friendsPhoto = document.querySelector(".friendsPhoto");
            const friendsName = document.querySelector(".friendsName");
            const friendsProfile = document.querySelector(".friendsProfile");

            friendsPhoto.setAttribute("src", playerSocial.imgSrc);
            friendsPhoto.setAttribute("alt", playerSocial.playerId);
            friendsName.innerHTML = playerSocial.playerName;
            friendsProfile.id = playerSocial.playerId;
            friendsProfile.style.display = "inline-block";

            friendsProfile.addEventListener("click", (event) => {
                location.href = `/protected/player/profile/${playerSocial.playerId}`;
            });
            
            enroll.value = chatId;
            roomNumber.value = chatId;
            await loadConversation(chatId);
            document.querySelector(".join #join").click();
            
            console.log("chatId = ", chatId);
            chatRoomForm.dataset.id = chatId;
        })
    }
}



const socket = io.connect()

document.querySelector('.join button').addEventListener('click', () => {
    console.log('hi1')
    console.log(myId)
    socket.emit('i-wanna-join-room', document.querySelector('.join input').value);
})

document.querySelector('#communicationForm').addEventListener("submit", async (event) => {
    event.preventDefault();
    console.log('hi3')
    // const res = await fetch("/protected/chatRoom/chatRooms", { method: "GET" });
    // const chatRooms = await res.json();
    const chatRooms = await loadChatRooms();
    event.preventDefault;
    console.log('hi5');
    // const player1Id = translatePlayerNameToPlayerId(chatRoomForm.me.value);
    // const player2Id = translatePlayerNameToPlayerId(chatRoomForm.friends.value);
    const player1Id = chatRoomForm.me.value;
    const player2Id = chatRoomForm.friends.value;
    await fetch("/protected/chatRoom/communicate", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({
            message: document.querySelector('.send input.message').value,
            player1Id: player1Id,
            player2Id: player2Id
        })
    });
    socket.emit('msg', document.querySelector('.send input.room').value, document.querySelector('.send input.message').value);
    document.querySelector('.send input.message').value = "";
});

socket.on('got-msg', async (msg) => {
    console.log('hi6')
    // const res = await fetch("/protected/chatRoom/chatRooms", { method: "GET" });
    // const chatRooms = await res.json();
    const chatRooms = await loadChatRooms();
    // const player1Id = translatePlayerNameToPlayerId(chatRoomForm.me.value);
    // const player2Id = translatePlayerNameToPlayerId(chatRoomForm.friends.value);
    // await loadConversation(findChatRoomIdId(myId, player2Id, chatRooms));
    console.log("chatRoomForm.dataset.id = ", chatRoomForm.dataset.id);
    await loadConversation(chatRoomForm.dataset.id);
    
    const convoScroll = document.querySelector(".convoScroll");
    convoScroll.scrollTop = convoScroll.scrollHeight;
});

socket.on('refresh-contact', async () => {
    console.log('bye4');
    await loadContact();
    console.log('bye5');
    updateDialogAfterPressingConnect();
    console.log('bye6');
})





const chatRoomForm = document.querySelector('#chatRoomForm');
chatRoomForm.addEventListener("submit", async (event) => {
    console.log('bye1')
    event.preventDefault();
    console.log('bye2')
    const res = await fetch("/protected/chatRoom/conversation", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({
            // player1Id:  translatePlayerNameToPlayerId(chatRoomForm.me.value),
            // player2Id:  translatePlayerNameToPlayerId(chatRoomForm.friends.value)
            player1Id:  chatRoomForm.me.value, 
            player2Id:  chatRoomForm.friends.value
        })
    });
    const json = await res.json();
    console.log("json = ", json);
    const chatRoomId = json.chatRoomId;
    console.log("chatRoomId = ", chatRoomId);
    chatRoomForm.dataset.id = chatRoomId;
    console.log("chatRoomForm.dataset.id = ", chatRoomForm.dataset.id);
    socket.emit('update-contact')
})

