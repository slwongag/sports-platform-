let player;

module.exports = async function loadPlayer(overwrite = false) {

    console.log("overwrite === true: ", overwrite === true);
    if (overwrite === true) {

        // do not check if player already exists in local storage 
        const res = await fetch("/protected/player/info", { method: "GET" });
        const playerData = await res.json();
        console.log("playerData = ", playerData);
        localStorage.setItem("player", JSON.stringify(playerData));
        
    } else {

        // check if player already exists in local storage first 
        console.log(`localStorage.getItem("player") = `, localStorage.getItem("player"));
        console.log(`localStorage.getItem("player") == null: `, localStorage.getItem("player") == null);

        if (localStorage.getItem("player") == null) {
            const res = await fetch("/protected/player/info", { method: "GET" });
            const playerData = await res.json();
            console.log("playerData = ", playerData);
            localStorage.setItem("player", JSON.stringify(playerData));
        }

    }

    console.log(`JSON.parse(localStorage.getItem("player")) = `, JSON.parse(localStorage.getItem("player")));
    console.log(`JSON.parse(localStorage.getItem("player")).playerId = `, JSON.parse(localStorage.getItem("player")).playerId);

    // player = JSON.parse(localStorage.getItem("player"));
    return JSON.parse(localStorage.getItem("player"));

}