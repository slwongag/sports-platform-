window.onload = async () => {
  player = await loadPlayer();
}


let player;

async function loadPlayer(overwrite = false) {

    console.log("overwrite === true: ", overwrite === true);
    if (overwrite === true) {

        // do not check if player already exists in local storage 
        const res = await fetch("/protected/player/info", { method: "GET" });
        const playerData = await res.json();
        console.log("playerData = ", playerData);
        localStorage.setItem("player", JSON.stringify(playerData));
        
    } else {

        // check if player already exists in local storage first 
        console.log(`localStorage.getItem("player") = `, localStorage.getItem("player"));
        console.log(`localStorage.getItem("player") == null: `, localStorage.getItem("player") == null);

        if (localStorage.getItem("player") == null) {
            const res = await fetch("/protected/player/info", { method: "GET" });
            const playerData = await res.json();
            console.log("playerData = ", playerData);
            localStorage.setItem("player", JSON.stringify(playerData));
        }

    }

    console.log(`JSON.parse(localStorage.getItem("player")) = `, JSON.parse(localStorage.getItem("player")));
    console.log(`JSON.parse(localStorage.getItem("player")).playerId = `, JSON.parse(localStorage.getItem("player")).playerId);

    // player = JSON.parse(localStorage.getItem("player"));
    return JSON.parse(localStorage.getItem("player"));

}


const mapForm = document.querySelector("#matchForm");
mapForm.addEventListener("submit", async (event) => {
  event.preventDefault();
  location.href="/protected/matchRecord.html"
  await fetch("/protected/match/matchInfo", {
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify({

      player1Id: matchForm.player1Id.value,
      player2Id: matchForm.player2Id.value,
      player1Name: player.playerName,
      player2Name: matchForm.player2Name.value,
      fromTime: matchForm.fromTime.value,
      toTime: matchForm.toTime.value,
      location: matchForm.location.value,
      sports: matchForm.sports.value,
      remarks: matchForm.remarks.value
    }) 
  });
})

//datepicker
$(function () {
    $('#datetimepicker7').datetimepicker();
    $('#datetimepicker8').datetimepicker({
        useCurrent: false
    });
    $("#datetimepicker7").on("change.datetimepicker", function (e) {
        $('#datetimepicker8').datetimepicker('minDate', e.date);
    });
    $("#datetimepicker8").on("change.datetimepicker", function (e) {
        $('#datetimepicker7').datetimepicker('maxDate', e.date);
    });
});


async function getMyPlayerName(){
  const res = await fetch("/protected/player/info", {method:"GET"});

  const player = await res.json();

  document.querySelector("#player1Name").value = player.playerName;}
getMyPlayerName();

function inPutPlayerId(){
    document.querySelector("#player1Id").value = window.location.search.split("&")[0].split("=")[1]
    document.querySelector("#player2Id").value = window.location.search.split("&")[1].split("=")[1]
    document.querySelector("#sports").value = window.location.search.split("&")[2].split("=")[1]
    const player2Name = window.location.search.split("&")[3].split("=")[1].split('%20').join(' ');
    document.querySelector("#player2Name").value = player2Name;
}
inPutPlayerId()