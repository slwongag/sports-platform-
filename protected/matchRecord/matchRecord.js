window.onload = async () => {
  player = await loadPlayer();
  GetMyUserId();
  loadMyMatches();
  loadReceivedMatches();
}


let player;

async function loadPlayer(overwrite = false) {

  console.log("overwrite === true: ", overwrite === true);
  if (overwrite === true) {

    // do not check if player already exists in local storage 
    const res = await fetch("/protected/player/info", { method: "GET" });
    const playerData = await res.json();
    console.log("playerData = ", playerData);
    localStorage.setItem("player", JSON.stringify(playerData));

  } else {

    if (localStorage.getItem("player") == null) {
      const res = await fetch("/protected/player/info", { method: "GET" });
      const playerData = await res.json();
      localStorage.setItem("player", JSON.stringify(playerData));
    }

  }
  return JSON.parse(localStorage.getItem("player"));
}

let myPlayerId;
async function GetMyUserId() {
  console.log(player.playerId)
  myPlayerId = player.playerId
  console.log(myPlayerId)
}

async function loadMyPendingMatches() {
  const res = await fetch(`/protected/match/loadMyMatches/pending`);
  const matches = await res.json();
  let pendingHtml = '';
  if (matches.length === 0){
      const showPendingMatch = document.querySelector("#mPendingMatch")
      showPendingMatch.innerHTML = "No Record"
      console.log(matches.length)
      const myPendingRow = document.querySelector("#myPendingRow")
      myPendingRow.style.display = "none"
  }else { 
      myPendingRow.style.display = "flex"
  console.log(matches.length)
  for (i = 0; i < matches.length; i++) {
      
      pendingHtml += `
              <form>
              <div class="matchPendingRow" id="${matches[i].matchId}">
              <div class="matchId">${matches[i].matchId}</div>
              <div class="matchType">${matches[i].sports}</div>
              <div class="matchPlayerName">${matches[i].player2Name}</div>
              <div class="matchStartTime ${matches[i].matchId}" id="startTime${matches[i].matchId}">${matches[i].fromTime}</div>
              <div class="matchEndTime ${matches[i].matchId}" id="endTime${matches[i].matchId}">${matches[i].toTime}</div>
              <div class="matchLocation ${matches[i].matchId}" id="location${matches[i].matchId}">${matches[i].location}</div>
              <div class="matchRemarks ${matches[i].matchId}" id="matchRemarks${matches[i].matchId}">${matches[i].remarks}</div>
              <div class="matchStatus">${matches[i].status}</div>
              <div class="matchEdit"><input type="button" id="${matches[i].matchId}button" value ="Edit" class="btn btn-primary pButton" onclick='editMatch("${matches[i].matchId}")'></div>
              <div class="matchReply"><button type="button" class="btn btn-success" id="cancelButton" onclick='cancelMatch("${matches[i].matchId}")'>Cancel</button></div>
              </div>
              </form>
              `
      const showPendingMatch = document.querySelector("#mPendingMatch")
      showPendingMatch.innerHTML = pendingHtml

      // const buttons = document.querySelectorAll('.pButton')
      // for (let button of buttons){
      //   console.log(button)
      //   button.addEventListener('click', editMatch())
      // }
      
    }
  }
}

async function editMatch(matchId){
  const editMatchInfoClass = document.querySelectorAll(`.${matchId}`);

  // for (let editMatchInfo of editMatchInfoClass){
  // editMatchInfo.contentEditable = 'true';}
  // console.log("editMatchInfo")

  const editSaveButton = document.querySelector(`#${matchId}button`)
  const matchFromTime = document.querySelector(`#startTime${matchId}`)
  const matchEndTime = document.querySelector(`#endTime${matchId}`)
  const matchLocation = document.querySelector(`#location${matchId}`)
  const matchRemarks = document.querySelector(`#matchRemarks${matchId}`)
  if (editSaveButton.value == 'Edit'){
    console.log(editSaveButton.value)
    
    for (let editMatchInfo of editMatchInfoClass){
    editMatchInfo.contentEditable = 'true';
  }
  editSaveButton.value = "Save";
  }
  else if (editSaveButton.value == 'Save'){
    await fetch(`/protected/match/updateMatchInfo/${matchId}`, {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify({
        matchId : matchId,
        fromTime : matchFromTime.innerHTML,
        toTime : matchEndTime.innerHTML,
        location : matchLocation.innerHTML,
        remarks :matchRemarks.innerHTML
    })
    });
    for (let editMatchInfo of editMatchInfoClass){
      editMatchInfo.contentEditable = 'false';
    }
    editSaveButton.value = "Edit";
    loadMyMatches();
    loadReceivedMatches();
  }
//   const editMatchInfo = document.querySelector("#completed");
// editMatchInfo.contentEditable = 'true';
}


async function loadMyAcceptedMatches() {
  const res = await fetch(`/protected/match/loadMyMatches/accepted`);
  const matches = await res.json();
  let acceptedHtml = '';
  if (matches.length === 0){
    const showAcceptedMatch = document.querySelector("#mAcceptedMatch")
      showAcceptedMatch.innerHTML = "No Record"
      const myAcceptedRow = document.querySelector("#myAcceptedRow")
      myAcceptedRow.style.display = "none"
    }
      else { 
        myAcceptedRow.style.display = "flex"
  for (i = 0; i < matches.length; i++) {
    let matchId = matches[i].matchId;
    let myPlayerId = matches[i].player1Id;
    let opponentId = matches[i].player2Id;
    let sport = matches[i].sports
    acceptedHtml += `
                <form id="${matches[i].matchId}">
                <div class="matchAcceptedRow" id="${matches[i].matchId}">
                <div class="matchAId">${matches[i].matchId}</div>
                <div class="matchType">${matches[i].sports}</div>
                <div class="matchAPlayerName">${matches[i].player2Name}</div>
                <div class="matchAStartTime">${matches[i].fromTime}</div>
                <div class="matchAEndTime">${matches[i].toTime}</div>
                <div class="matchALocation">${matches[i].location}</div>
                <div class="matchARemarks">${matches[i].remarks}</div>
                <div class="matchAStatus">${matches[i].status}</div>
                <div class="matchAResult">
                <input type="text" id="mySetNum" placeholder="Your score">
                <input type="text" id="oppSetNum" placeholder="Opponent's score">
                </div>
                <div class="matchType">
                <select id="setNum" required>
                  <option selected>Choose...</option>
                  <option value="First">1</option>
                  <option value="Third">3</option>
                </select>
                </div>
                <div class="submitAReply">
                <button type="submit" class="btn btn-success resultButton" onclick='submitMatchResult("${matchId}", "${myPlayerId}","${opponentId}","${sport}")'>Submit</button>
                </div>
                </form>

                </div>`
        const showAcceptedMatch = document.querySelector("#mAcceptedMatch")
        showAcceptedMatch.innerHTML = acceptedHtml
    }
  }
}

async function loadMyCompletedMatches() {
  const res = await fetch(`/protected/match/loadMyMatches/completed`);
  const matches = await res.json();
  let completedHtml = '';
  if (matches.length === 0){
      const showCompletedMatch = document.querySelector("#mCompletedMatch")
      showCompletedMatch.innerHTML = "No Record"
      const myCompletedRow = document.querySelector("#myCompletedRow")
      myCompletedRow.style.display = "none"
  }else { 
      myCompletedRow.style.display = "flex"
  for (i = 0; i < matches.length; i++) {
      console.log(matches[i].matchId)
      let result = ""
      if (matches[i].player1SetNum > matches[i].player2SetNum) {
        result = `<i class="far fa-grin-beam"></i> Win ${matches[i].player1SetNum}:${matches[i].player2SetNum}`
      } else if (matches[i].player1SetNum < matches[i].player2SetNum) {
        result = `<i class="far fa-dizzy"></i> Lose ${matches[i].player1SetNum}:${matches[i].player2SetNum}`
      }
      completedHtml += `<div class="matchCompletedRow" id="${matches[i].matchId}">
                        <div class="matchCId">${matches[i].matchId}</div>
                        <div class="matchType">${matches[i].sports}</div>
                        <div class="matchCPlayerName">${matches[i].player2Name}</div>
                        <div class="matchCStartTime">${matches[i].fromTime}</div>
                        <div class="matchCEndTime">${matches[i].toTime}</div>
                        <div class="matchCLocation">${matches[i].location}</div>
                        <div class="matchCRemarks">${matches[i].remarks}</div>
                        <div class="matchCStatus">${matches[i].status}</div>
                        <div class="matchCResult">${result}</div>
                        </div>`
      const showCompletedMatch = document.querySelector("#mCompletedMatch")
      showCompletedMatch.innerHTML = completedHtml
    }
  }
}

async function loadMyCancelledMatches() {
  const res = await fetch(`/protected/match/loadMyMatches/cancelled`);
  const matches = await res.json();
  let cancelledHtml = '';
  if (matches.length === 0){
    const showCancelledMatch = document.querySelector("#mCancelledMatch")
    showCancelledMatch.innerHTML = "No Record"
    const myCancelledMatchRow = document.querySelector("#myCancelledMatchRow")
    myCancelledMatchRow.style.display = "none"
    }else { 
      myCancelledMatchRow.style.display = "flex"
      for (i = 0; i < matches.length; i++) {
      cancelledHtml += `<div class="matchCancelledRow" id="${matches[i].matchId}">
                        <div class="matchCId">${matches[i].matchId}</div>
                        <div class="matchType">${matches[i].sports}</div>
                        <div class="matchCPlayerName">${matches[i].player2Name}</div>
                        <div class="matchCStartTime">${matches[i].fromTime}</div>
                        <div class="matchCEndTime">${matches[i].toTime}</div>
                        <div class="matchCLocation">${matches[i].location}</div>
                        <div class="matchCRemarks">${matches[i].remarks}</div>
                        <div class="matchCStatus">${matches[i].status}</div>
                        </div>`
    const showCancelledMatch = document.querySelector("#mCancelledMatch")
    showCancelledMatch.innerHTML = cancelledHtml
     }
  }
}

async function loadReceivedPendingMatches() {
  const res = await fetch(`/protected/match/loadReceivedMatches/pending`);
  const matches = await res.json();
  let receivedPendingHtml = '';
  if (matches.length === 0){
      const showReceivedPendingMatch = document.querySelector("#iPendingMatch")
      showReceivedPendingMatch.innerHTML = "No Record"
      const iPendingMatchRow = document.querySelector("#iPendingMatchRow")
      iPendingMatchRow.style.display = "none"
      }else { 
        iPendingMatchRow.style.display = "flex"
        console.log(matches.length)
        for (i = 0; i < matches.length; i++) {
      
      receivedPendingHtml += `
                              <form>
                              <div class="matchPendingRow" id="${matches[i].matchId}">
                              <div class="matchId">${matches[i].matchId}</div>
                              <div class="matchType">${matches[i].sports}</div>
                              <div class="matchPlayerName">${matches[i].player2Name}</div>
                              <div class="matchStartTime ${matches[i].matchId}" id="startTime${matches[i].matchId}">${matches[i].fromTime}</div>
                              <div class="matchEndTime ${matches[i].matchId}" id="endTime${matches[i].matchId}">${matches[i].toTime}</div>
                              <div class="matchLocation ${matches[i].matchId}" id="location${matches[i].matchId}">${matches[i].location}</div>
                              <div class="matchRemarks ${matches[i].matchId}" id="matchRemarks${matches[i].matchId}">${matches[i].remarks}</div>
                              <div class="matchStatus">${matches[i].status}</div>
                              <div class="matchEdit"><input type="button" id="${matches[i].matchId}button" value ="Edit" class="btn btn-primary pButton" onclick='editMatch("${matches[i].matchId}")'></div>
                      <div class="matchRcAccept"><button type="button" class="btn btn-success" id="acceptButton" onclick='acceptMatch("${matches[i].matchId}")'>Accept</button></div>
                      <div class="matchRcDecline"><button type="button" class="btn btn-danger" id="declineButton" onclick='cancelMatch("${matches[i].matchId}")'>Decline</button></div>
              
              </div>
              `
      const showReceivedPendingMatch = document.querySelector("#iPendingMatch")
      showReceivedPendingMatch.innerHTML = receivedPendingHtml
    }
  }
}


async function loadReceivedAcceptedMatches() {
  const res = await fetch(`/protected/match/loadReceivedMatches/accepted`);
  const matches = await res.json();
  let receivedAcceptedHtml = '';
  if (matches.length === 0){
    const showReceivedAcceptedMatch = document.querySelector("#iAcceptedMatch")
      showReceivedAcceptedMatch.innerHTML = "No Record"
      const iAcceptedMatchRow = document.querySelector("#iAcceptedMatchRow")
      iAcceptedMatchRow.style.display = "none"
      }else { 
        iAcceptedMatchRow.style.display = "flex"
        for (i = 0; i < matches.length; i++) {
        let matchId = matches[i].matchId;
        let myPlayerId = matches[i].player2Id;
        let opponentId = matches[i].player1Id;
        let sport = matches[i].sports
        receivedAcceptedHtml += `
                              <form id="${matches[i].matchId}">
                              <div class="matchAcceptedRow" id="${matches[i].matchId}">
                              <div class="matchAId">${matches[i].matchId}</div>
                              <div class="matchType">${matches[i].sports}</div>
                              <div class="matchAPlayerName">${matches[i].player1Name}</div>
                              <div class="matchAStartTime">${matches[i].fromTime}</div>
                              <div class="matchAEndTime">${matches[i].toTime}</div>
                              <div class="matchALocation">${matches[i].location}</div>
                              <div class="matchARemarks">${matches[i].remarks}</div>
                              <div class="matchAStatus">${matches[i].status}</div>
                              <div class="matchAResult">
                              <input type="text" id="oppSetNum" placeholder="Your score">
                              <input type="text" id="mySetNum" placeholder="Opponent's score">
                              </div>
                              <div class="matchType">
                              <select id="setNum" required>
                                <option selected>Choose...</option>
                                <option value="First">1</option>
                                <option value="Third">3</option>
                              </select>
                              </div>
                              <div class="submitAReply">
                              <button type="submit" class="btn btn-success resultButton" onclick='submitMatchResult("${matchId}", "${myPlayerId}","${opponentId}","${sport}")'>Submit</button>
                              </div>
                              </form>

                              </div>`
        const showReceivedAcceptedMatch = document.querySelector("#iAcceptedMatch")
        showReceivedAcceptedMatch.innerHTML = receivedAcceptedHtml
    }
  }
}

async function loadReceivedCompletedMatches() {
  const res = await fetch(`/protected/match/loadReceivedMatches/completed`);
  const matches = await res.json();
  let receivedCompletedHtml = '';
  if (matches.length === 0){
    const showReceivedCompletedMatch = document.querySelector("#iCompletedMatch")
    showReceivedCompletedMatch.innerHTML = "No Record"
    const iCompletedMatchRow = document.querySelector("#iCompletedMatchRow")
    iCompletedMatchRow.style.display = "none"
    }else { 
      iCompletedMatchRow.style.display = "flex"
  for (i = 0; i < matches.length; i++) {
      console.log(matches[i].matchId)
      let result = ""
      if (matches[i].player1SetNum > matches[i].player2SetNum) {
        result = `<i class="far fa-dizzy"></i> Lose ${matches[i].player2SetNum}:${matches[i].player1SetNum}`
      } else if (matches[i].player1SetNum < matches[i].player2SetNum) {
        result = `<i class="far fa-grin-beam"></i> Win ${matches[i].player2SetNum}:${matches[i].player1SetNum}`
      }
      receivedCompletedHtml += `<div class="matchCompletedRow" id="${matches[i].matchId}">
                                <div class="matchCId">${matches[i].matchId}</div>
                                <div class="matchType">${matches[i].sports}</div>
                                <div class="matchCPlayerName">${matches[i].player1Name}</div>
                                <div class="matchCStartTime">${matches[i].fromTime}</div>
                                <div class="matchCEndTime">${matches[i].toTime}</div>
                                <div class="matchCLocation">${matches[i].location}</div>
                                <div class="matchCRemarks">${matches[i].remarks}</div>
                                <div class="matchCStatus">${matches[i].status}</div>
                                <div class="matchCResult">${result}</div>
                                </div>`
      const showReceivedCompletedMatch = document.querySelector("#iCompletedMatch")
      showReceivedCompletedMatch.innerHTML = receivedCompletedHtml
      }
    }
}

async function loadReceivedCancelledMatches() {
  const res = await fetch(`/protected/match/loadReceivedMatches/cancelled`);
  const matches = await res.json();
  let receivedCancelledHtml = '';
  if (matches.length === 0){
    const showReceivedCancelledMatch = document.querySelector("#iCancelledMatch")
    showReceivedCancelledMatch.innerHTML = "No Record"
    const iDeclinedMatchRow = document.querySelector("#iDeclinedMatchRow")
    iDeclinedMatchRow.style.display = "none"
    }else { 
      iDeclinedMatchRow.style.display = "flex"
  for (i = 0; i < matches.length; i++) {
    receivedCancelledHtml += `<div class="matchDeclinedRow" id="${matches[i].matchId}">
                              <div class="matchCId">${matches[i].matchId}</div>
                              <div class="matchType">${matches[i].sports}</div>
                              <div class="matchCPlayerName">${matches[i].player1Name}</div>
                              <div class="matchCStartTime">${matches[i].fromTime}</div>
                              <div class="matchCEndTime">${matches[i].toTime}</div>
                              <div class="matchCLocation">${matches[i].location}</div>
                              <div class="matchCRemarks">${matches[i].remarks}</div>
                              <div class="matchCStatus">${matches[i].status}</div>
                              </div>`
    const showReceivedCancelledMatch = document.querySelector("#iCancelledMatch")
    showReceivedCancelledMatch.innerHTML = receivedCancelledHtml
    }
  }
}
  
function loadMyMatches(){
  loadMyPendingMatches();
  loadMyCompletedMatches();
  loadMyAcceptedMatches();
  loadMyCancelledMatches();
}

function loadReceivedMatches(){
  loadReceivedPendingMatches();
  loadReceivedCompletedMatches();
  loadReceivedAcceptedMatches();
  loadReceivedCancelledMatches();
}


async function cancelMatch (matchId) {

  await fetch(`/protected/match/cancelMatch/${matchId}`, {
    method: 'PUT',
    headers: {
      "Content-Type": "application/json; charset=utf-8"
  },
  body: JSON.stringify({
      status: "cancelled"
  })
  });
  loadMyMatches();
  loadReceivedMatches();
}

async function acceptMatch (matchId) {

  await fetch(`/protected/match/acceptMatch/${matchId}`, {
    method: 'PUT',
    headers: {
      "Content-Type": "application/json; charset=utf-8"
  },
  body: JSON.stringify({
      status: "accepted"
  })
  });
  loadReceivedMatches();
}

// async function editMatch (matchId) {

//   await fetch(`/protected/match/editMatch/${matchId}`, {
//     method: 'PUT',
//     headers: {
//       "Content-Type": "application/json; charset=utf-8"
//   },
//   body: JSON.stringify({
      
//   })
//   });
//   loadMyMatches();
//   loadReceivedMatches();
// }

async function submitMatchResult (matchId, myId, oppId, sport) {
  //*Upload Match Result//
  const form = document.querySelector(`#${matchId}`)
  form.addEventListener("submit", async (event) => {
  event.preventDefault();
  await fetch(`/protected/match/updateMatchResult/${matchId}`, {
    method: 'PUT',
    headers: {
      "Content-Type": "application/json; charset=utf-8"
  },
  body: JSON.stringify({
      formOfMatch : form.setNum.value,
      mySetNum : form.mySetNum.value,
      oppSetNum : form.oppSetNum.value,
      status: "finished"
  })
  });
  // Get myElo and oppElo//
  const rank = await fetch(`/leaderboard/${sport}`)
  const players = await rank.json();
  const oppRecord = players.find(oppPlayer => oppPlayer.playerId === oppId);
  const myRecord = players.find(player => player.playerId === myId);
  console.log(oppRecord)
  console.log(myRecord)

  for (let i = 0; i < oppRecord.sports.length; i++) {
          if (oppRecord.sports[i].sport === sport ){
            oppElo = oppRecord.sports[i].proficiency
            oppTotalMatch = oppRecord.sports[i].record.match
          }
        }
          console.log(oppElo)
          console.log(oppTotalMatch)

   for (let i = 0; i < myRecord.sports.length; i++) {
            if (myRecord.sports[i].sport === sport ){
              myElo = myRecord.sports[i].proficiency
              myTotalMatch =  myRecord.sports[i].record.match
            }
          }
            console.log(myElo)
            console.log(myTotalMatch)

//  *Cal my new elo*//
  const calMyRank = await fetch('/protected/algo',  {
  method: "POST", 
  headers: {
    "Content-Type": "application/json; charset=utf-8"
  },
  body: JSON.stringify({
    myScore: myElo,
    competitorScore: oppElo,
    formOfMatch : form.setNum.value,
    mySetNum : form.mySetNum.value,
    oppSetNum : form.oppSetNum.value,
    myFirstTen : myTotalMatch
  })
  })
  const myNewElo = await calMyRank.json();
  console.log(myNewElo)
//  *Cal opp new elo*//
  const calOppRank = await fetch('/protected/algo',  {
    method: "POST", 
    headers: {
      "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify({
      myScore: oppElo,
      competitorScore: myElo,
      formOfMatch : form.setNum.value,
      mySetNum : form.mySetNum.value,
      oppSetNum : form.oppSetNum.value,
      myFirstTen : oppTotalMatch
    })
    })
    const oppNewElo = await calOppRank.json();
    console.log(oppNewElo)
  // * updatePlayers
  await fetch(`/protected/player/updateProficiency/`, {
    method: 'PUT',
    headers: {
      "Content-Type": "application/json; charset=utf-8"
  },
  body: JSON.stringify({
    player1Id: myId,
    player2Id: oppId,
    sport: sport,
    myNewElo : myNewElo,
    oppNewElo : oppNewElo,
    mySetNum : form.mySetNum.value,
    oppSetNum : form.oppSetNum.value,
  })
  });
  loadMyMatches();
  loadReceivedMatches();
  })
}


// const editMatchInfo = document.querySelector("#completed");
// editMatchInfo.contentEditable = 'true';