window.onload = async () => {
  player = await loadPlayer();
  GetMyUserId();
}

let player;

async function loadPlayer(overwrite = false) {

  console.log("overwrite === true: ", overwrite === true);
  if (overwrite === true) {
    
    // do not check if player already exists in local storage 
    const res = await fetch("/protected/player/info", { method: "GET" });
    const playerData = await res.json();
    console.log("playerData = ", playerData);
    localStorage.setItem("player", JSON.stringify(playerData));
    
  } else {
    
    // check if player already exists in local storage first 
    // console.log(`localStorage.getItem("player") = `, localStorage.getItem("player"));
    // console.log(`localStorage.getItem("player") == null: `, localStorage.getItem("player") == null);
  
    if (localStorage.getItem("player") == null) {
      const res = await fetch("/protected/player/info", { method: "GET" });
      const playerData = await res.json();
      // console.log("playerData = ", playerData);
      localStorage.setItem("player", JSON.stringify(playerData));
    }

  }

  // console.log(`JSON.parse(localStorage.getItem("player")) = `, JSON.parse(localStorage.getItem("player")));
  // console.log(`JSON.parse(localStorage.getItem("player")).playerId = `, JSON.parse(localStorage.getItem("player")).playerId);

  return JSON.parse(localStorage.getItem("player"));

}



// const logoutButton = document.querySelector("#logout");

// logoutButton.addEventListener("click", async (event) => {
//     event.preventDefault();

//     const res = await fetch("/logout");
//     const json = await res.json();

//     console.log("window.location.pathname = ", window.location.pathname);
//     if (window.location.pathname !== '/login' && window.location.pathname !== '/login.html') {
//         window.location.href = "/login";
//     }
// })
function show() {

  document.getElementById("formDiv").style.display = "block";
}

function hide() {
  document.getElementById("formDiv").style.display = "none";
}

let myPlayerId;
async function GetMyUserId() {
  // const res = await fetch("/protected/player/info", { method: "GET" });

  // const player = await res.json();
  console.log(player.playerId)
  myPlayerId = player.playerId
  console.log(myPlayerId)
}


const matchingForm = document.querySelector("#matchForm");
matchingForm.addEventListener("submit", async (event) => {
  event.preventDefault();
  let availability = [
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null],
    [null, null]
  ];
  const timeSlots = document.querySelectorAll(".time-slot");
  console.log("timeSlots = ", timeSlots);
  for (let timeSlot of timeSlots) {
    const timeSlotId = timeSlot.id;
    // console.log("timeSlotId = ", timeSlotId);
    const timeSlotIdValues = timeSlot.id.substring(timeSlot.id.length - 2);
    // console.log("timeSlotIdValues = ", timeSlotIdValues);
    availability[parseInt(timeSlotIdValues[0], 10)][parseInt(timeSlotIdValues[1], 10)] = timeSlot.checked ? 1 : 0;
    // console.log("timeSlot.checked = ", timeSlot.checked);
  }
  console.log(availability)
  const res = await fetch("/protected/matching", {

    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify({

      chosenSports: matchForm.chosenSports.value,
      chosenGender: matchForm.gender.value,
      minimum: matchForm.ageMin.value,
      maximum: matchForm.ageMax.value,
      chosenDistance: matchForm.chosenDistance.value,
      weightingOfProficiency: 100 - matchForm.PriorityRange.value,
      weightingOfDistance: matchForm.PriorityRange.value,
      availability: availability
    })
  });
  const matchedPlayers = await res.json();
  console.log(matchedPlayers[0])
  let html = "";
  if (matchedPlayers[0] != undefined) {
    for (let j = 0; j < matchedPlayers.length; j++) {
      for (let i = 0; i < matchedPlayers[j].sports.length; i++) {

        let winNum = matchedPlayers[j].sports[i].record.win
        let loseNum = matchedPlayers[j].sports[i].record.lose
        let playerName = matchedPlayers[j].playerName
        let elo = matchedPlayers[j].sports[i].proficiency
        let winRatio = Math.round(winNum / (winNum + loseNum) * 100)
        let age = matchedPlayers[j].age
        let gender = matchedPlayers[j].gender
        let playerId = matchedPlayers[j].playerId
        let profilePic = matchedPlayers[j].imgSrc


        html += `
      <div class="row addedRow">
      <div class="col-3" id="playerProfile"><img id="profilePic" src="${profilePic}"
          alt="Player profile picture"></div>
      <div class="col-4 playerContent" id="playerInformation">Name : ${playerName} </br> Age : ${age} <br> Gender : ${gender} </div>
      <div class="col-4 playerContent" id="matchRecord"> Proficiency : ${elo}</br> Match : ${winNum}W / ${loseNum}L </br> Win rate : ${winRatio}%</div>
      <div class="col-1 playerContent"id="createMatch"><button type="button" onclick='createMatch("${myPlayerId}", "${playerId}", "${playerName}", "${matchForm.chosenSports.value}")' class="btn btn-success">Invite</button></div>
      </div>`
      }
      const showPlayer = document.querySelector('#showPlayers')
      document.getElementById("matched-rules").style.display = "block";
      showPlayer.innerHTML = html;
      showPlayer.scrollIntoView();
    }
  }
  else {
    html = ''
    const showPlayer = document.querySelector('#showPlayers')
    showPlayer.innerHTML = html;
    window.alert("No player found, please adjust your setting")
  }

})

function createMatch(Id1, Id2, name, chosenSports){
  location.href=`/protected/matchSetup/matchInfo.html?player1Id=${Id1}&player2Id=${Id2}&sports=${chosenSports}&player2Name=${name}`
}

document.querySelector("#challengeButton").addEventListener("click", ()=>{
  location.href="/leaderBoard"
  }
);
