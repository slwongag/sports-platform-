import * as express from "express";
import { LeaderBoardService} from "../services/LeaderBoardService";
import { leaderBoardService } from "../app"


export class LeaderBoardRouter {
    private leaderBoardService: LeaderBoardService

    constructor() {
        this.leaderBoardService = leaderBoardService;
    }

    public router(){

    const router = express.Router();

    router.get("/:sports", this.RankAllPlayerBySportAndProficiency);
    router.get("/" , (req: express.Request, res: express.Response) => res.redirect("/leaderboard.html"))

    return router;
    }
    
    // [CODE REVIEW] naming. camelCase
    public RankAllPlayerBySportAndProficiency = (req: express.Request, res: express.Response) => {
    res.json(this.leaderBoardService.RankAllPlayerBySportAndProficiency(req.params.sports));
    };
}
   
