import * as express from "express";
import { algoService } from "../app"
import { AlgoService } from "../services/AlgoService"

export class AlgoRouter{

    private algoService : AlgoService;

    constructor (){
        this.algoService = algoService;
    }   

    public router(){
        const router = express.Router();
        router.post("/", this.eloAlgo);
        return router;
    }

    public eloAlgo = (req: express.Request, res: express.Response) => {
        res.json(this.algoService.eloAlgo( 
            req.body.myScore, 
            req.body.competitorScore, 
            req.body.mySetNum ,
            req.body.oppSetNum, 
            req.body.formOfMatch, 
            req.body.myTotalMatch
            )
            );
    }

}