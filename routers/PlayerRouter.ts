import * as express from "express";
import * as multer from "multer";
import * as path from "path";
import * as moment from "moment";
import { playerService } from "../app";
import { PlayerService, Player, Sport } from "../services/PlayerService";

export class PlayerRouter {

    protected playerService: PlayerService;

    constructor() {
        this.playerService = playerService;
    }

    public router() {

        const storage = multer.diskStorage({
            destination: (req, file, cb) => {
                // [CODE REVIEW] Add __dirname
                cb(null, "protected/uploads/");
            }, 
            filename: (req, file, cb) => {
                cb(null, `${req.user.userId}-${moment().format("YYYYMMDD-HHmmss")}.${file.mimetype.split('/')[1]}`);
            }
        })
        const upload = multer({storage});

        const router = express.Router();

        router.get("/sports", this.loadSports);
        router.get("/players",this.loadPlayers);
        router.get("/playersSocial",this.loadPlayersSocial);
        router.get("/profile", this.viewProfile);
        router.get("/profile/:playerId", this.getPublicProfile);
        router.post("/profile", this.addPlayer);
        router.get("/info", this.getPlayer);
        router.post("/edit", this.editPlayer);
        router.post("/uploadProfilePic", upload.single('uploadImg'), this.updatePlayerImgSrc);
        router.put("/updatePlayerInfo", this.updatePlayerInfo);
        router.put("/updateTimeLocation", this.updatePlayerTimeLocation);
        router.put("/updateProficiency", this.updatePlayerProficiencyAfterMatch);
        router.post("/joinSport", this.playerAddSport);

        return router;
    }

    protected loadSports = (req: express.Request, res: express.Response) => {
        try {
            const sports = this.playerService.loadSports();
            res.json(sports);
        } catch(err) {
            console.log(err.message);
            // [CODE REVIEW] Add res.status(500) or res.status(400)
            res.json({result: err.message});
        }
    }

    protected loadPlayers = (req: express.Request, res: express.Response) => {
        try {
            const players = this.playerService.loadPlayers();
            res.json(players);
        } catch(err) {
            console.log(err.message);
            res.json({result: err.message});
        }
    }

    protected loadPlayersSocial = (req: express.Request, res: express.Response) => {
        try {
            const playersSocial = this.playerService.loadPlayersSocial();
            res.json(playersSocial);
        } catch(err) {
            console.log(err.message);
            res.json({result: err.message});
        }
    }

    protected viewProfile = (req: express.Request, res: express.Response) => {
        res.redirect("/protected/profile");
    }

    protected getPublicProfile = (req: express.Request, res: express.Response) => {
        try {
            const publicProfile = this.playerService.getPublicProfile(req.params.playerId);
            res.json(publicProfile);
        } catch(err) {
            console.log(err.message);
            res.json({result: err.message});
        }
    }

    protected getPlayer = (req: express.Request, res: express.Response) => {
        try {
            const player = this.playerService.getPlayer(req.user.userId);
            if (player) {
                res.json(player);
            } else {
                res.json(null);
            }
        } catch (err) {
            console.log("player not found");
            res.json({ result: "player not found" });
        }
    }

    protected addPlayer = (req: express.Request, res: express.Response) => {
        let info: Sport[] = [];
        for (let sport of req.body.sports) {
            info.push({
                sport: sport, 
                proficiency: 800, 
                record: {
                    match: 0, 
                    win: 0, 
                    lose: 0
                }
            });
        }
        try {
            let imgSrc;
            if (req.body.gender == 'M') {
                imgSrc = "/empty-profile-M_edited.jpg";
            } else if (req.body.gender == 'F') {
                imgSrc = "/empty-profile-F_edited.jpg";
            } else {
                console.log("req.body.gender = ", req.body.gender);
                imgSrc = "";
            }
            const player: Player = {
                playerId: req.user.userId, 
                playerName: req.body.playerName, 
                age: req.body.age, 
                gender: req.body.gender, 
                contacts: { phone: req.body.phoneNumber, email: req.body.email }, 
                availability: req.body.availability, 
                location: [req.body.latitude, req.body.longitude], 
                sports: info, 
                imgSrc: imgSrc, 
                sortNumber: 0
            }
            console.log(req.user.userId);
            this.playerService.addPlayer(player);
        } catch (err) {
            console.log(err.message);
            res.json({ result: err.message });
        }
    }

    protected editPlayer = (req: express.Request, res: express.Response) => {
        try {
            // [CODE REVIEW] No need further JSON parse
            this.playerService.editPlayer(req.body.editedPlayer);
            res.json({result: "Player's info edited!"});
        } catch(err) {
            console.log(err.message);
            res.json({result: err.message});
        }
    }

    protected updatePlayerImgSrc = (req: express.Request, res: express.Response) => {
        try {
            this.playerService.updatePlayerImgSrc(req.user.userId, path.join("/protected/uploads/", req.file.filename));
            res.json({result: "Upload successfully!"});
        } catch(err) {
            console.log(err.message);
            res.json({result: "Upload failed!"});
        }
    }
    
    protected updatePlayerInfo = (req: express.Request, res: express.Response) => {
        try {
            this.playerService.updatePlayerInfo(req.user.userId, req.body.editedPlayerName, req.body.editedContacts);
            res.json({result: "Upload successfully!"});
        } catch(err) {
            console.log(err.message);
            res.json({result: "Upload failed!"});
        }
    }

    protected updatePlayerTimeLocation = (req: express.Request, res: express.Response) => {
        try {
            this.playerService.updatePlayerTimeLocation(req.user.userId, req.body.availability, [req.body.latitude, req.body.longitude]);
            res.json({result: "Upload successfully!"});
        } catch(err) {
            console.log(err.message);
            res.json({result: "Upload failed!"});
        }
    }

    protected updatePlayerProficiencyAfterMatch = (req: express.Request, res: express.Response) =>{
        try {
            // [CODE REVIEW] potential manipulated elo by client here.
            // [CODE REVIEW] should use algo service to recalculate / directly calculate
            this.playerService.updatePlayerProficiencyAfterMatch(req.body.player1Id, req.body.player2Id, req.body.sport, req.body.myNewElo, req.body.oppNewElo, req.body.mySetNum, req.body.oppSetNum);
            res.json({result: "Upload successfully!"});
        } catch(err) {
            console.log(err.message);
            res.json({result: "Upload failed!"});
        }
    }
        
    protected playerAddSport = (req: express.Request, res: express.Response) => {
        try {
            const newSport = {
                sport: req.body.sport, 
                proficiency: 800, 
                record: {
                    match: 0, 
                    win: 0, 
                    lose: 0
                }
            };
            this.playerService.playerAddSport(req.user.userId, newSport);
            res.json({result: `You have successfully joined ${req.body.sport}!`});
        } catch(err) {
            console.log(err.message);
            res.json({result: `Failed to join ${req.body.sport}!`});
        }
    }
}