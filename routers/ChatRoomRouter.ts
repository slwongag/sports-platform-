import * as express from "express";
import { chatRoomService } from "../app";
import { ChatRoomService } from "../services/ChatRoomService";

export class ChatRoomRouter {
    protected chatRoomService: ChatRoomService;

    constructor() {
        this.chatRoomService = chatRoomService;
    }

    public router() {
        const router = express.Router();

        router.get("/chatRooms", this.loadChatRooms)
        router.post("/conversation", this.createChatRoom);
        router.post("/communicate", this.communicate);

        return router;
    }

    protected loadChatRooms = (req: express.Request, res: express.Response) => {
        try {
            const chatRooms = this.chatRoomService.loadChatRooms();
            res.json(chatRooms);
        } catch (err) {
            console.log(err.message);
            res.json({ result: err.message });
        }
    }

    protected createChatRoom = (req: express.Request, res: express.Response) => {
        try {
            const chatRoomId = this.chatRoomService.createChatRoom(
                req.body.player1Id, req.body.player2Id
            );
            res.json({ chatRoomId: chatRoomId });
        }
        catch (err) {
            console.log(err.message);
            res.json({ result: err.message });
        }
    }

    protected communicate = (req: express.Request, res: express.Response) => {
        try {
            this.chatRoomService.addContent(
                req.body.message, req.body.player1Id, req.body.player2Id
            );
            res.json();
        }
        catch (err) {
            console.log(err.message);
            res.json({ result: err.message });
        }
    }






}