import * as express from "express";
import { matchInfoRouter, algoRouter } from "../app";
import { playerRouter, matchRouter,chatRoomRouter } from "../app";
import { playerService } from "../app";


export class ProtectedRouter {

    router() {
        const router = express.Router();

        // [CODE REVIEW] CheckProfileMiddleware cannot protect the html
        router.get("/main", checkProfileMiddleware, (req: express.Request, res: express.Response) => res.redirect("/protected/main.html"));
        router.get("/profile", checkProfileMiddleware, (req: express.Request, res: express.Response) => res.redirect("/protected/profile.html"));
        router.get("/profileSetup", checkProfileMiddleware, (req: express.Request, res: express.Response) => res.redirect("/protected/profileSetup.html"));
        router.get("/match", checkProfileMiddleware, (req: express.Request, res:express.Response) => res.redirect("/protected/matchSetup/matchInfo.html"));
        router.get("/record", checkProfileMiddleware, (req: express.Request, res:express.Response) => res.redirect("/protected/matchRecord.html"));
        router.get("/chatRoom",checkProfileMiddleware,(req: express.Request, res:express.Response) => res.redirect("/protected/chatRoom.html"));
        router.use("/algo", algoRouter.router());
        router.use("/player", playerRouter.router());
        router.use("/match", matchInfoRouter.router());
        router.use("/matching", matchRouter.router());
        router.use("/chatRoom",chatRoomRouter.router());
        
        return router;
    }

}

// [CODE REVIEW] Put this back to ProtectedRouter
const checkProfileMiddleware = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.log("checkProfileMiddleware is called");
    const player = playerService.getPlayer(req.user.userId);
    console.log("player = ", player);
    const setProfilePath = (req.path == '/profileSetup/' || req.originalUrl == '/protected/profileSetup');
    console.log("req.path = ", req.path);
    console.log("req.originalUrl = ", req.originalUrl);
    console.log("setProfilePath = ", setProfilePath);
    if (player && setProfilePath) {
        // player already exists!
        console.log("Your profile already exists! \nPlease edit your profile details on the profile page!");
        res.redirect("/protected/profile");
    } else if (!player && !setProfilePath) {
        // player not created yet!
        console.log("player not created yet!");
        res.redirect("/protected/profileSetup");
    } else {
        // no problem
        console.log("no problem!");
        next();
    }
}
