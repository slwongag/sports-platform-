import * as express from "express";
import { matchInfoService } from "../app"
import { MatchInfoService } from "../services/MatchInfoService"

export class MatchInfoRouter {

    private matchInfoService: MatchInfoService;

    constructor() {
        this.matchInfoService = matchInfoService;
    }

    public router() {
        const router = express.Router();
        
        router.post("/matchInfo", this.createMatch);
        router.get("/loadMyMatches", this.loadMyMatches);
        router.get("/loadMyMatches/:status", this.loadMyMatchesByStatus);
        router.get("/loadReceivedMatches/:status", this.loadReceivedMatchesByStatus);
        router.put("/updateMatchResult/:matchId", this.updateMatchResult)
        router.get("/loadReceivedMatches", this.loadReceivedMatches);
        router.get("/loadReceivedMatches/:status", this.loadReceivedMatchesByStatus);
        router.put("/cancelMatch/:matchId", this.cancelMatch)
        router.put("/acceptMatch/:matchId", this.acceptMatch)
        router.put("/updateMatchInfo/:matchId", this.updateMatchInfo)
        // router.put("/updateMatchResult", this.updateMatchResult);
        // router.put("/updateMatchStatus", this.updateMatchStatus);
        
        return router;
    }

    protected createMatch = (req: express.Request, res: express.Response) => {
        this.matchInfoService.createMatch(
            req.user.userId, 
            req.body.player2Id, 
            req.body.player1Name,
            req.body.player2Name,
            req.body.fromTime, 
            req.body.toTime, 
            req.body.location, 
            req.body.sports, 
            req.body.remarks
        );
    }

    protected loadMyMatches = (req: express.Request, res: express.Response) =>{
        res.json(this.matchInfoService.findMyMatchesByPlayerId(req.user.userId));
    }

    protected loadMyMatchesByStatus = (req: express.Request, res: express.Response) =>{
        res.json(this.matchInfoService.findMyMatchesByStatus(req.user.userId, req.params.status));
    } 

    protected loadReceivedMatches = (req: express.Request, res: express.Response) =>{
        res.json(this.matchInfoService.findReceivedMatchesByPlayerId(req.user.userId));
    }

    protected loadReceivedMatchesByStatus = (req: express.Request, res: express.Response) =>{
        res.json(this.matchInfoService.findReceivedMatchesByStatus(req.user.userId, req.params.status));
    }

    protected cancelMatch = (req: express.Request, res: express.Response) => {
        try {
            this.matchInfoService.cancelMatchById(
                req.params.matchId, 
                req.body.status);
            res.json({result: "Updated successfully!"});
        } catch(err) {
            console.log(err.message);
            res.json({result: "Updated failed!"});
        }
    }

    protected acceptMatch = (req: express.Request, res: express.Response) => {
        try {
            this.matchInfoService.acceptMatchById(
                req.params.matchId, 
                req.body.status);
            res.json({result: "Updated successfully!"});
        } catch(err) {
            console.log(err.message);
            res.json({result: "Updated failed!"});
        }
    }

    protected updateMatchResult = (req:express.Request, res: express.Response) =>{
        try {
        this.matchInfoService.updateMatchResult(
            req.params.matchId, 
            req.body.formOfMatch,
            req.body.mySetNum,
            req.body.oppSetNum
        );
        res.json({result: "Updated successfully!"});
    }  catch(err) {
         console.log(err.message);
    res.json({result: "Updated failed!"});
        }
    }  

    protected updateMatchInfo = (req:express.Request, res: express.Response) =>{
        try {
        this.matchInfoService.updateMatchInfo(
            req.params.matchId, 
            req.body.fromTime,
            req.body.toTime,
            req.body.location,
            req.body.remarks
        );
        res.json({result: "Updated successfully!"});
    }  catch(err) {
         console.log(err.message);
    res.json({result: "Updated failed!"});
        }
    }  
}