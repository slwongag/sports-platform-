import * as express from "express";
import { matchService } from "../app";
import { MatchService } from "../services/MatchService";

export class MatchRouter {

    private matchService: MatchService;

    constructor() {
        this.matchService = matchService;
    }

    public router() {
        const router = express.Router();
        
        router.post("/", this.filterThenSort);
        
        return router;
    }

    protected filterThenSort = (req: express.Request, res: express.Response) => {
        res.json(this.matchService.filterThenSort(
            req.body.chosenSports, 
            req.body.chosenGender, 
            req.body.minimum, 
            req.body.maximum, 
            req.user.userId, 
            req.body.chosenDistance,
            req.body.availability,
            req.body.weightingOfProficiency, 
            req.body.weightingOfDistance, 
            )
        );
        console.log(this.matchService.filterThenSort(
            req.body.chosenSports, 
            req.body.chosenGender, 
            req.body.minimum, 
            req.body.maximum, 
            req.user.userId, 
            req.body.chosenDistance,
            req.body.availability,
            req.body.weightingOfProficiency, 
            req.body.weightingOfDistance, 
            ))
    }
}