import * as express from "express";
import { userService } from "../app";
import { UserService } from "../services/UserService";

export class UserRouter {

    private userService: UserService;
    
    constructor() {
        this.userService = userService;
    }

    public router() {
        const router = express.Router();

        router.get("/loginStatus", this.getLoginStatus);
        router.post("/register", this.register);
        router.get("/details", this.getUser);
        
        return router;
    }
    
    // [CODE REVIEW] Unused code
    protected getUsers = (req: express.Request, res: express.Response) => {
        res.json(this.userService.getUsers());
    }

    protected getUser = (req: express.Request, res: express.Response) => {
        res.json(req.user);
    }
    
    protected getLoginStatus = (req: express.Request, res: express.Response) => {
        console.log("req.user = ", req.user);
        console.log("req.user !== undefined: ", req.user !== undefined);
        // [CODE REVIEW] May just return the current user
        res.json(req.user !== undefined);
    }

    protected register = (req: express.Request, res: express.Response) => {
        console.log("req.body.username = ", req.body.username);
        console.log("req.body.password = ", req.body.password);
        const user = this.userService.findUserByUsername(req.body.username);
        if (user) {
            console.log("Username already taken!");
            res.json({approval: false, message: "Username already taken!"});
        } else {
            this.userService.addUser(req.body.username, req.body.password);
            console.log("account registered");
            res.json({approval: true, message: "Account registered! Please log in!"});
        }
    }

    protected logout = (req: express.Request, res: express.Response) => {
        if (req.session) {
            req.session.authenticated = false;
            console.log("req.session.authenticated = ", req.session.authenticated);
            res.json({loggedIn: false});
        } else {
            res.json({loggedIn: false});
        }
    }

}