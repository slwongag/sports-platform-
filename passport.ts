import * as passport from "passport";
import * as passportLocal from "passport-local";
import * as express from "express";
import { checkPassword } from "./hash";
import { userService } from "./app";
import { User } from "./services/UserService";

const LocalStrategy = passportLocal.Strategy;

// adopt local strategy
passport.use(new LocalStrategy(
    async function (username, password, done) {
        const users = userService.getUsers();
        const user = users.find(user => user.username === username);
        console.log("user = ", user);
        if (!user) {
            // done(error, user, info)
            done(null, false, {message: "invalid_username"});
        } else {
            const authenticated = await checkPassword(password, user.password);
            console.log("authenticated = ", authenticated);
            if (!authenticated) {
                // done(error, user, info)
                done(null, false, {message: "invalid_password"});
            } else {
                // done(error, user, info)
                done(null, user);
            }
        }
    }
));

// serialize user - store minimal info (userId) passport for identification
passport.serializeUser(function (user: User, done) {
    done(null, user.userId);
});

// deserialize user - use userId stored in passport to identify user
passport.deserializeUser(async function (id, done) {
    const users = await userService.getUsers();
    const user = users.find(user => id === user.userId);
    if (user) {
        done(null, user);
    } else {
        done(new Error("User not found"));
    }
});

// specify login flow
export function loginFlow(req: express.Request, res: express.Response, next: express.NextFunction) {
    return (err: Error, user: User, info: {message: string}) => {
        if (err) {
            // error in login
            console.log("/login?error=" + err.message);
            res.json({access: false, message: "Login failed!"});    
            // res.redirect("/login?error=" + err.message);    
        } else if (info && info.message) {
            // info exists - invalid username or password
            console.log("/login?error=" + info.message);
            res.json({access: false, message: "Invalid Username or Password!"});    
            // res.redirect("/login?error=" + info.message);
        } else {
            // login success
            req.logIn(user, (err) => {
                if (err) {
                    // error
                    console.log("/login?error=" + err.message);
                    res.json({access: false, message: "Login failed!"});    
                    // res.redirect("/login?error=" + err.message);
                } else {
                    // no error
                    console.log("/login success");
                    res.json({access: true, message: "welcome!"});    
                    // res.redirect("/protected/main");
                }
            });
        }
    };
}

// specify route guard
export function loginGuard(req: express.Request, res: express.Response, next: express.NextFunction) {
    console.log("loginGuard is called");
    if (req.user) {
        next();
    } else {
        res.redirect("/login");
    }
}