window.onload = async () => {
  // await GetMyUserId();  // without try-catch, it will crash and stop here
  await loadLeaderBoard("tennis");
  await changeSport("tennis");
  await changeSport("badminton");
  await changeSport("football");
  // hideChallenge();
}

// let player;

// async function loadPlayer(overwrite = false) {

//   console.log("overwrite === true: ", overwrite === true);
//   if (overwrite === true) {

//     // do not check if player already exists in local storage 
//     const res = await fetch("/protected/player/info", { method: "GET" });
//     const playerData = await res.json();
//     console.log("playerData = ", playerData);
//     localStorage.setItem("player", JSON.stringify(playerData));

//   } else {

//     // check if player already exists in local storage first 
//     // console.log(`localStorage.getItem("player") = `, localStorage.getItem("player"));
//     // console.log(`localStorage.getItem("player") == null: `, localStorage.getItem("player") == null);

//     if (localStorage.getItem("player") == null) {
//       const res = await fetch("/protected/player/info", { method: "GET" });
//       const playerData = await res.json();
//       // console.log("playerData = ", playerData);
//       localStorage.setItem("player", JSON.stringify(playerData));
//     }

//   }

//   // console.log(`JSON.parse(localStorage.getItem("player")) = `, JSON.parse(localStorage.getItem("player")));
//   // console.log(`JSON.parse(localStorage.getItem("player")).playerId = `, JSON.parse(localStorage.getItem("player")).playerId);

//   return JSON.parse(localStorage.getItem("player"));

// }

let myPlayerId;
async function GetMyUserId() {
  // [CODE REVIEW] add try catch for non logged in user
  try {
    const res = await fetch("/protected/player/info", { method: "GET" });

    const playerData = await res.json();
    console.log("playerData = ", playerData);
    myPlayerId = playerData ? playerData.playerId : null;
  } catch (e) {
    myPlayerId = null;
  }
  
}
GetMyUserId();

function hideChallenge() {
  const navbarProfile = document.querySelector("#navbarProfile");
  const navbarRecord = document.querySelector("#navbarRecord");
  const navbarChatRoom = document.querySelector("#navbarChatRoom");
  const logout = document.querySelector("#logout");
  if (!myPlayerId) {
    console.log("myPlayerId = ", myPlayerId);
    // const challengeButton = document.querySelector("#challengeButton")
    // challengeButton.style.display = "none"
    navbarProfile.parentElement.style.display = "none";
    navbarRecord.parentElement.style.display = "none";
    navbarChatRoom.parentElement.style.display = "none";
    logout.style.display = "none";
    
    const createMatches = document.querySelectorAll(".playerButton");
    for (let createMatch of createMatches) {
      console.log("myPlayerId = ", myPlayerId);
      const challengeButton = document.querySelector("#challengeButton");
      challengeButton.style.display = "none";
      createMatch.style.display = "none";
    }
  } else {
    navbarProfile.parentElement.style.display = "block";
    navbarRecord.parentElement.style.display = "block";
    navbarChatRoom.parentElement.style.display = "block";
    logout.style.display = "block";
  }
}
hideChallenge();


async function loadLeaderBoard(sports) {
  const res = await fetch(`/leaderBoard/${sports}`);
  const leaderBoard = await res.json();
  let html = '';
  for (let j = 0; j < leaderBoard.length; j++) {
    for (let i = 0; i < leaderBoard[j].sports.length; i++) {
      let winNum = leaderBoard[j].sports[i].record.win
      let loseNum = leaderBoard[j].sports[i].record.lose
      let playerName = leaderBoard[j].playerName
      let elo = leaderBoard[j].sports[i].proficiency
      let winRatio = Math.round(winNum / (winNum + loseNum) * 100)
      let loadPlayerId = leaderBoard[j].playerId
      let sport = leaderBoard[j].sports[i].sport

      html += `
            <div class="row playerInformation" id="${loadPlayerId}">
                <div class="playerRank">${j + 1}</div>
                <div class="playerName">${playerName}</div>
                <div class="playerRecord">Win: ${winNum} Lose: ${loseNum}</div>
                <div class="playerRatio">${winRatio}%</div>
                <div class="playerScore">${elo}</div>
                <div class="playerButton" id="createMatch"><button type="button" onclick='createMatch("${myPlayerId}", "${loadPlayerId}", "${sport}", "${playerName}")' class="btn btn-success">Challenge</button></div>
            </div>
            `;
      const showPlayer = document.querySelector('#ldTable');
      showPlayer.innerHTML = html;
      const sportIcons = document.querySelectorAll('.sportIcon');
      for (let sportIcon of sportIcons) {
        sportIcon.style.color = "white";
      }
      const sportColor = document.querySelector(`#${sports}`);
      sportColor.style.color = "#55ff88";
      await hideChallenge();
    }
  }
}



function createMatch(Id1, Id2, sport, playerName){
    location.href=`/protected/matchSetup/matchInfo.html?player1Id=${Id1}&player2Id=${Id2}&sports=${sport}&player2Name=${playerName}`
  }


async function changeSport(sports) {

  const sport = document.querySelector(`#${sports}`);

  sport.addEventListener("click", async (event) => {
    await loadLeaderBoard(sports);
    await GetMyUserId();
  })

}