async function checkLogin() {
    const res = await fetch("/user/loginStatus");
    const loginStatus = await res.json();
    console.log("loginStatus = ", loginStatus);

    if (loginStatus) {
        // logged in => redirect to fs page
        console.log("logged in already");
        location.href = "/protected/main";
    } else {
        // not logged in yet => redirect to login page 
        console.log("not logged in yet");
        location.href = "/login";
    }
}

checkLogin();