const loginTab = document.querySelector("#loginTab");
const registerTab = document.querySelector("#registerTab");

loginTab.addEventListener("click", () => {
    loginTab.classList.add("active");
    registerTab.classList.remove("active");
});

registerTab.addEventListener("click", () => {
    registerTab.classList.add("active");
    loginTab.classList.remove("active");
});

async function login(username, password) {
    const res = await fetch("/login", {
        method: "POST", 
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        }, 
        body: JSON.stringify({
            username: username, 
            password: password
        })
    });
    const authentication = await res.json();
    console.log("authentication = ", authentication);
    if (authentication.access) {
        // correct username & password
        console.log(authentication.message);
        location.href = "/protected/main";
        // await redirect();
    } else {
        // incorrect username & password
        console.log(authentication.message);
        const info = document.querySelector("#info");
        info.innerHTML = `
            <div class="icon icon-alert">
                <i class="fas fa-exclamation"></i>
            </div>
            <p>${authentication.message}</p>
        `;
        await fetch("/logout", {method: "GET"});
    }
}

// async function redirect() {
//     const res = await fetch("/user/details", {method: "GET"});
//     const user = await res.json();
//     console.log("user = ", user);

//     const res1 = await fetch("/protected/player/" + user.userId, {method: "GET"});
//     const player = await res1.json();
//     console.log("player = ", player);

//     if (player) {
//         location.href = "/protected/main";
//     } else {
//         location.href = "/protected/profileSetup";
//     }
// }

async function register(username, password) {
    const res = await fetch("/user/register", {
        method: "POST", 
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        }, 
        body: JSON.stringify({
            username: username, 
            password: password
        })
    });
    const registration = await res.json(); 
    console.log("registration = ", registration);
    const info = document.querySelector("#info");
    if (registration.approval) {
        info.innerHTML = `
            <div class="icon">
                <i class="fas fa-check"></i>
            </div>
            <p>${registration.message}</p>
        `;
    } else {
        info.innerHTML = `
            <div class="icon icon-alert">
                <i class="fas fa-exclamation"></i>
            </div>
            <p>${registration.message}</p>
        `;
    }
}

const userForm = document.querySelector("#userForm");
userForm.addEventListener("submit", async (event) => {
    event.preventDefault();
    console.log(`loginTab.classList.contains("active") = ${loginTab.classList.contains("active")}`);
    console.log(`registerTab.classList.contains("active") = ${registerTab.classList.contains("active")}`);
    if (loginTab.classList.contains("active") && !registerTab.classList.contains("active")) {
        // user login
        console.log("user login protocol");
        await login(userForm.username.value, userForm.password.value);
    } else if (!loginTab.classList.contains("active") && registerTab.classList.contains("active")) {
        // user register
        console.log("user register protocol");
        await register(userForm.username.value, userForm.password.value);        
    } else {
        // something went wrong!!
        console.log("something went wrong!!");
    }
});

// window.onload = function() {
//     const searchParams = new URLSearchParams(window.location.search);
//     const errMessage = searchParams.get("error");

//     if (errMessage) {
//         const alertBox = document.createElement("div");
//         alertBox.classList.add("alert", "alert-danger");
//         alertBox.textContent = errMessage;
//         document.querySelector("#info").appendChild(alertBox);
//     } else {
//         document.querySelector("#info").innerHTML = "";
//     }
// }