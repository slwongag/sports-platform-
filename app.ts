import * as express from "express";
import * as expressSession from "express-session";
import * as bodyParser from "body-parser";
import * as http from "http";
import * as socketIO from "socket.io";
import * as passport from "passport";
import * as path from "path";
import * as moment from "moment";
import { loginFlow, loginGuard } from "./passport";
import { UserRouter } from "./routers/UserRouter";
import { UserService } from "./services/UserService";
import { ProtectedRouter } from "./routers/ProtectedRouter";
import { PlayerRouter } from "./routers/PlayerRouter";
import { PlayerService } from "./services/PlayerService";
import { MatchInfoRouter } from "./routers/MatchInfoRouter";
import { MatchInfoService} from "./services/MatchInfoService";
import { MatchRouter } from "./routers/MatchRouter";
import { MatchService} from "./services/MatchService";
import { LeaderBoardService} from "./services/LeaderBoardService";
import { LeaderBoardRouter} from "./routers/LeaderBoardRouter";
import { AlgoService} from "./services/AlgoService"
import { AlgoRouter} from "./routers/AlgoRouter"
import { ChatRoomService} from "./services/ChatRoomService";
import { ChatRoomRouter} from "./routers/ChatRoomRouter";
import "./passport";

const app = express();
const server = new http.Server(app);
const io = socketIO(server);
const PORT = 8000;
export const userService = new UserService();
export const playerService = new PlayerService();
export const matchInfoService = new MatchInfoService();
export const matchService = new MatchService();
export const leaderBoardService = new LeaderBoardService();
export const leaderBoardRouter = new LeaderBoardRouter();
export const algoService = new AlgoService();
export const chatRoomService = new ChatRoomService();
export const chatRoomRouter = new ChatRoomRouter();


export const protectedRouter = new ProtectedRouter();
export const userRouter = new UserRouter();
export const playerRouter = new PlayerRouter();
export const matchInfoRouter = new MatchInfoRouter();
export const matchRouter = new MatchRouter();
export const algoRouter = new AlgoRouter();

// specify sessionMiddleware
const sessionMiddleware = expressSession({
    secret: "everyone knows", 
    resave: true, 
    saveUninitialized: true, 
    cookie: {secure: false}
});

// backend request log on node
app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.log("[" + moment().format("YYYY-MM-DD HH:mm:ss") + "] " + req.path);
    next();
});

// use sessionMiddleware for express
app.use(sessionMiddleware);

// use bodyParser for express
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// use sessionMiddleware for socketIO
io.use((socket, next) => {
    sessionMiddleware(socket.request, socket.request.res, next);
});

// initialize socketIO connection
io.on("connection", function (socket) {
    console.log(socket);
    // store socket.id as socketId inside session
    // [CODE REVIEW] become problematic if a user opens more than 1 tab
    // socket.request.session.socketId = socket.id;
    socket.request.session.save();
    // destroy socketId when disconnected
    socket.on("disconnect", () => {
        socket.request.session.socketId = null;
        socket.request.session.save();
    })
    socket.on ('i-wanna-join-room',(room)=>{
        console.log('hi2')
        socket.join(room);
    })
    socket.on ('msg',(room,msg)=>{
        console.log('hi4')
        io.to(room).emit('got-msg',msg);
    })
    socket.on('update-contact',()=>{
        console.log('bye3')
        io.emit('refresh-contact')
    })
});

// passport - application middleware
// initialize passport
app.use(passport.initialize());
// notify passport of the usage of session
app.use(passport.session());

// grant access to public folder
app.use(express.static(__dirname + "/public"));
// grant access to resources folder
app.use(express.static(__dirname + "/resources"));
// protected access to protected folder with loginGuard
app.use("/protected", loginGuard, express.static(__dirname + "/protected"));

// Routers
app.use("/user", userRouter.router());
app.use("/protected", protectedRouter.router());
app.use("/leaderBoard", leaderBoardRouter.router());
   

// Route Handlers
// login routes
app.get("/login", (req: express.Request, res: express.Response) => {
    res.sendFile(path.join(__dirname, "/public/login.html"));
});
app.post("/login", (...rest) => {
    // passport.authenticate("local", loginFlow(...rest))(...rest);
    const authMiddleware = passport.authenticate("local", loginFlow(...rest));
    authMiddleware(...rest);
});
// app.post("/login", passport.authenticate("local", {failureRedirect: "/logout"}), (req: express.Request, res: express.Response) => {
//     res.redirect("/protected/main");
// });
// main route
// app.get("/protected/main", (req: express.Request, res: express.Response) => {
//     res.sendFile(path.join(__dirname, "/protected/main.html"));
// });

// logout route
app.get("/logout", (req: express.Request, res: express.Response, next: express.NextFunction) => {
    req.logout();
    res.json({loggedOut: true});
});


// http server listens to the PORT
server.listen(PORT, () => console.log(`Listening to http://localhost:${PORT}...`));