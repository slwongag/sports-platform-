import * as bcrypt from "bcrypt";

const SALT_ROUNDS = 10;

export async function hashPassword(plainPassword: string) {
    const hash = await bcrypt.hash(plainPassword, SALT_ROUNDS);
    return hash;
}

export async function checkPassword(plainPassword: string, hashPassword: string) {
    const match = await bcrypt.compare(plainPassword, hashPassword);
    return match;
}



// // Test Codes
// async function test() {
//     console.log('hashPassword("111") = ', await hashPassword("111"));
//     // hashPassword("111") =  $2b$10$iO0xrC3IsWA2XmC8pEso3.nYAHQUWH0R7uQkXRZ5OETjG6XzmhEjG
//     console.log('hashPassword("222") = ', await hashPassword("222"));
//     // hashPassword("222") =  $2b$10$rgCyWxN51oduOnTTJtIojuRykZIKyoTBK974GxIRzcg86rDUEH3fy
//     console.log('hashPassword("333") = ', await hashPassword("333"));
//     // hashPassword("333") =  $2b$10$SD4UBfhQyjktMMfFG/E3uOcub.uTbFbIYpaOtFFJyrfBjvO25L.vu
// }
// test();