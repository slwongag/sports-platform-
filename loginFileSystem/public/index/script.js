async function checkLogin() {
    const res = await fetch("/user/loginStatus");
    const json = await res.json();

    if (json.loggedIn) {
        // logged in => redirect to fs page
        console.log("logged in already");
        location.href = "/protected/fs.html";
    } else {
        // not logged in yet => redirect to login page 
        console.log("not logged in yet");
        location.href = "/login.html";
    }

}

checkLogin();