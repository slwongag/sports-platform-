const loginTab = document.querySelector("#loginTab");
const registerTab = document.querySelector("#registerTab");

loginTab.addEventListener("click", (event) => {
    // event.preventDefault();
    loginTab.classList.add("active");
    registerTab.classList.remove("active");
});

registerTab.addEventListener("click", (event) => {
    // event.preventDefault();
    registerTab.classList.add("active");
    loginTab.classList.remove("active");
});

const userForm = document.querySelector("#userForm");
userForm.addEventListener("submit", async (event) => {
    event.preventDefault();
    console.log(`loginTab.classList.contains("active") = ${loginTab.classList.contains("active")}`);
    console.log(`registerTab.classList.contains("active") = ${registerTab.classList.contains("active")}`);
    if (loginTab.classList.contains("active") && !registerTab.classList.contains("active")) {
        // user login
        const res = await fetch("/user/login", {
            method: "POST", 
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            }, 
            body: JSON.stringify({
                username: userForm.username.value, 
                password: userForm.password.value
            })
        });
        const access = await res.json();
        console.log("access = ", access);
        if (access.loggedIn) {
            location.href = "/protected/fs.html";
        } else {
            const info = document.querySelector("#info");
            info.innerHTML = `
                <div class="icon icon-alert">
                    <i class="fas fa-exclamation"></i>
                </div>
                <p>Incorrect username or password</p>
            `;
        }
    } else if (!loginTab.classList.contains("active") && registerTab.classList.contains("active")) {
        // user register
        const res = await fetch("/user/register", {
            method: "POST", 
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            }, 
            body: JSON.stringify({
                username: userForm.username.value, 
                password: userForm.password.value
            })
        });
        const approval = await res.json(); 
        console.log("approval = ", approval);
        const info = document.querySelector("#info");
        if (approval.registered) {
            info.innerHTML = `
                <div class="icon">
                    <i class="fas fa-check"></i>
                </div>
                <p>Account registered! Please log in!</p>
            `;
        } else {
            info.innerHTML = `
                <div class="icon icon-alert">
                    <i class="fas fa-exclamation"></i>
                </div>
                <p>Username already taken!</p>
            `;
        }
    } else {
        // something went wrong!!
        console.log("something went wrong!!");
    }
});