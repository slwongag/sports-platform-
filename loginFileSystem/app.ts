import * as express from "express";
import * as expressSession from "express-session";
import * as bodyParser from "body-parser";
import * as moment from "moment";
import { UserRouter } from "./routers/UserRouter";
import { FsRouter } from "./routers/FsRouter";

const app = express();
const PORT = 8000;
const userRouter = new UserRouter();
const fsRouter = new FsRouter();

const loginMiddleware = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.log("loginMiddleware is called");
    if (req.session && req.session.authenticated) {
        next();
    } else {
        // res.status(401).json({result: "please login"});
        res.redirect("/login.html");
    }
}

app.use(expressSession({
    secret: "everyone knows it", 
    resave: true, 
    saveUninitialized: true
}));

app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.log("[" + moment().format("YYYY-MM-DD HH:mm:ss") + "] " + req.path);
    next();
});

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/uploads"));
app.use('/protected', loginMiddleware, express.static(__dirname + "/protected"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use("/user", userRouter.router());
app.use("/fs", loginMiddleware, fsRouter.router());

app.listen(PORT, () => console.log(`Listening to http://localhost:${PORT}...`));