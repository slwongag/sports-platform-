const resultsList = document.querySelector("#resultsList");

async function renderResultRows(res) {
    console.log("typeof res = ", typeof res);
    console.log("res = ", res);

    let results = await res.json();
    console.log("typeof results = ", typeof results);
    console.log("results = ", results);
    
    if (!results instanceof Array || results.result) {
        results = Object.values(results);
    }
    console.log("typeof results = ", typeof results);
    console.log("results = ", results);

    let html = '';
    for (let result of results) {
        html += `<li class="list-group-item">${result}</li>`;
    }

    resultsList.innerHTML = html;
}

const directoriesForm = document.querySelector("#directoriesForm");
directoriesForm.addEventListener("submit", async (event) => {
    event.preventDefault();
    const res = await fetch("/fs/directories?path=" + directoriesForm.path.value, {method: "GET"});
    await renderResultRows(res);
});

const downloadForm = document.querySelector("#downloadForm");
downloadForm.addEventListener("submit", async (event) => {
    event.preventDefault();
    console.log("downloadForm.path.value = ", downloadForm.path.value);
    const res = await fetch("/fs/file?path=" + downloadForm.path.value, {method: "GET"});
    console.log("res = ", res);
    // try {
    // } catch(err) {
        // const res = JSON.stringify({result: "path not found"});
        // console.log("res = ", res);
    // }
    await renderResultRows(res);
});

const uploadForm = document.querySelector("#uploadForm");
uploadForm.addEventListener("submit", async (event) => {
    event.preventDefault();
    const uploadFormData = new FormData(uploadForm);
    console.log("uploadFormData = ", uploadFormData);
    const res = await fetch("/fs/file", {
        method: "POST", 
        body: uploadFormData
    });
    await renderResultRows(res);
});

const deleteForm = document.querySelector("#deleteForm");
deleteForm.addEventListener("submit", async (event) => {
    event.preventDefault();
    const res = await fetch("/fs?path=" + deleteForm.path.value, {method: "DELETE"});
    await renderResultRows(res);
});

const logoutButton = document.querySelector("#logoutButton");
logoutButton.addEventListener("click", async (event) => {
    event.preventDefault();

    await fetch("/user/logout", {method: "POST"});
    location.href = "/";
});

