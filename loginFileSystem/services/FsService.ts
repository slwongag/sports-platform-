import * as fs from "fs";

export class FsService {
    
    async listDirectories(targetPath: string) {
        return await fs.promises.readdir(targetPath);
    }

    async copyFile(original: string, destination: string) {
        return await fs.promises.copyFile(original, destination);
    }

    async renameFile(original: string, destination: string) {
        return await fs.promises.rename(original, destination);
    }

    async deleteFile(targetPath: string) {
        return await fs.promises.unlink(targetPath);
    }

}