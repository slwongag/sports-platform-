export class UserService {

    protected users: any[] = [
        {username: "user1", password: "111"}, 
        {username: "user2", password: "222"}, 
        {username: "user3", password: "333"}, 
    ];

    getUsers() {
        return this.users;
    }

    findUser(username: string, password: string) {
        const user = this.users.find(user => user.username === username && user.password === password);
        return user;
    }

    findUserByUsername(username: string) {
        const user = this.users.find(user => user.username === username);
        return user;
    }

    addUser(username: string, password: string) {
        if (this.findUserByUsername(username)) {
            throw new Error("username already taken");
        } else {
            this.users.push({username: username, password: password});
        }
    }
    
}