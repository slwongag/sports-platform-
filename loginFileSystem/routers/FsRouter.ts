import * as express from "express";
import * as multer from "multer";
import * as path from "path";
import { FsService } from "../services/FsService";

export class FsRouter {

    protected fsService = new FsService();

    public router() {
        const storage = multer.diskStorage({
            destination: (req, file, cb) => {
                cb(null, "uploads/");
            }, 
            filename: (req, file, cb) => {
                cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
            }
        })
        const upload = multer({storage});
        const router = express.Router();

        router.get("/directories", this.listDirectories);
        router.get("/file", this.downloadFile);
        router.post("/file", upload.single('file'), this.uploadFile);
        router.delete("/", this.deleteFile);

        return router;
    }

    protected listDirectories = async (req: express.Request, res: express.Response) => {
        try {
            console.log("req.query.path = ", req.query.path);
            res.json(await this.fsService.listDirectories(req.query.path));
        } catch(err) {
            console.log(err);
            res.json({result: "path not found"});
        }
    }

    protected downloadFile = (req: express.Request, res: express.Response) => {
        try {
            console.log("req.query.path = ", req.query.path);
            res.download(req.query.path);
        } catch(err) {
            console.log(err);
            res.json({result: "file not found"});
        }
    }

    protected uploadFile = async (req: express.Request, res: express.Response) => {
        try {
            console.log("req.body.path = ", req.body.path);
            console.log("req.file = ", req.file);
            // await this.fsService.copyFile(req.file.path, req.body.path + "/" + req.file.filename);
            await this.fsService.renameFile(req.file.path, path.join(req.body.path, req.file.filename));
            res.json({result: "file uploaded"});
        } catch(err) {
            console.log(err);
            res.json({result: "upload failed"});
        }
    }

    protected deleteFile = async (req: express.Request, res: express.Response) => {
        try {
            console.log("req.query.path = ", req.query.path);
            await this.fsService.deleteFile(req.query.path);
            res.json({result: "file deleted"});
        } catch(err) {
            console.log(err);
            res.json({result: "file not found"});
        }
    }

}