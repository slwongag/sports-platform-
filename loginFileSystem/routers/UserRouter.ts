import * as express from "express";
import { UserService } from "../services/UserService";

export class UserRouter {

    private userService: UserService = new UserService();

    public router() {
        const router = express.Router();

        // router.get("/login/users", this.getUsers);
        router.get("/loginStatus", this.getLoginStatus);
        router.post("/login", this.login);
        router.post("/register", this.register);
        router.post("/logout", this.logout);
        
        return router;
    }
    
    protected getUsers = (req: express.Request, res: express.Response) => {
        res.json(this.userService.getUsers());
    }

    protected getLoginStatus = (req: express.Request, res: express.Response) => {
        if (req.session && req.session.authenticated) {
            res.json({loggedIn: true});
        } else {
            res.json({loggedIn: false});
        }
    }

    protected login = (req: express.Request, res: express.Response) => {
        console.log("req.body.username = ", req.body.username);
        console.log("req.body.password = ", req.body.password);
        if (req.session && this.userService.findUser(req.body.username, req.body.password)) {
            req.session.authenticated = true;
            console.log("req.session.authenticated = ", req.session.authenticated);
            console.log("welcome back!");
            res.json({loggedIn: true});
        } else if (req.session) {
            req.session.authenticated = false;
            console.log("req.session.authenticated = ", req.session.authenticated);
            console.log("incorrect username or password");
            res.json({loggedIn: false});
        } else {
            console.log("incorrect username or password");
            res.json({loggedIn: false});
        }
    }

    protected register = (req: express.Request, res: express.Response) => {
        console.log("req.body.username = ", req.body.username);
        console.log("req.body.password = ", req.body.password);
        if (this.userService.findUserByUsername(req.body.username)) {
            console.log("username already taken");
            res.json({registered: false});
        } else {
            this.userService.addUser(req.body.username, req.body.password);
            console.log("account registered");
            res.json({registered: true});
        }
    }

    protected logout = (req: express.Request, res: express.Response) => {
        if (req.session) {
            req.session.authenticated = false;
            console.log("req.session.authenticated = ", req.session.authenticated);
            res.json({loggedIn: false});
        } else {
            res.json({loggedIn: false});
        }
    }

}