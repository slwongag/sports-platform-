require("ts-node/register");
require("./app");

//testing for input same js file and do push and pull

//Filter -->sports, age range, gender, time slot.
//Sorting = distance-->proficiency


// Project Architecture
// http://marklodato.github.io/js-boxdrawing/
//└─┬╴sports-platform-
//  ├──╴index.js
//  ├──╴app.ts
//  ├─┬╴Public
//  │ ├──╴index.html [/]
//  │ ├──╴login.html [/login] [logout]
//  │ ├─┬╴index
//  │ │ ├──╴index.js
//  │ │ └──╴style.css
//  │ └─┬╴login
//  │   └──╴login.js
//  ├─┬╴Protected
//  │ ├──╴profileSetup.html [/protected/profileSetup]
//  │ ├──╴profile.html [/protected/profile]
//  │ ├──╴message.html [/protected/message]
//  │ ├──╴leaderBoard.html [/protected/leaderBoard]
//  │ ├──╴matchInfo.html [/protected/matchInfo]
//  │ ├──╴main.html [/protected/main]
//  │ └─┬╴main
//  │   └──╴main.js
//  ├─┬╴Routers
//  │ ├──╴UserRouter.ts
//  │ ├──╴RankRouter.ts
//  │ ├──╴SortingRouter.ts
//  │ ├──╴MatchRouter.ts
//  │ ├──╴ChatRouter.ts
//  │ └──╴PlayerRouter.ts
//  └─┬╴Services
//    ├──╴UserService.ts
//    ├──╴RankService.ts
//    ├──╴SortingService.ts
//    ├──╴MatchService.ts
//    ├──╴ChatService.ts
//    └──╴PlayerService.ts





// Profile

// player
    // id: string
    // name: string
    // age: number
    // proficiency: number
    // location: [x: number, y: number]
    // availability
        // weekly: 7 days
        // daily: 4 time slots (6-hour period)
    // contacts: {phone: string, email: string}

